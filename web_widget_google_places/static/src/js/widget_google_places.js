/**
 * Created by Eezee-It on 14/8/15.
 */

/** Variable for google places api **/
var placesAutocomplete;

/** google api component form dictionary **/
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'short_name',
  postal_code: 'short_name'
};

openerp.web_widget_google_places = function (oe) {

    var _lt = oe.web._lt;

    /** Adds the new widget **/
    oe.web.form.widgets.add('google_places', 'openerp.web_widget_google_places.FieldTextGooglePlaces');

    /** function for pre-load the data for countries and states
    because .call and .query are very slows.
    TODO: improve this function with a fast ajax query **/
    function LoadData(model){
        var model = new oe.web.Model(model);
        var Data = {};
        model.call('search', [[]], {})
            .then(function (ids) {
                return model.call('read', [ids, ['name', 'id', 'code']]);
            })
            .then(function (data) {
                _(data).each(function(d){
                    Data[d.code] = [d.id, d.name];
                })
            });
        return Data;
    }

    /** Initialize DataObjects **/
    var Countries = LoadData("res.country");
    var States = LoadData("res.country.state");

    oe.web_widget_google_places.FieldTextGooglePlaces = oe.web.form.FieldChar.extend(
        {
            template: 'FieldGooglePlaces',
            display_name: _lt('GooglePlaces'),
            widget_class: 'oe_form_field_google_places',
            events: {
                'focus input': 'geolocate',
            },

            geolocate: function(){
                console.log("Geolocate!!");
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                      var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                      };

                      var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                      });
                      placesAutocomplete.setBounds(circle.getBounds());
                    });
                }
            },

            /** This function draw the field when the view is edit
            and when the record change **/
            render_value: function () {
                this.$txt = this.$el.find('input[name="' + this.name + '"]');
                var show_value = this.format_value(this.get('value'), '');
                var selector = this.field_manager.$el;
                var fields = this.field_manager.fields;
                var FormFields;
                var input_name;

                /** Fillfields are the fields that you want fill in the form **/
                for (var field in fields){
                    if (fields[field].widget == 'google_places'){
                        input_name = fields[field].name;
                        if (fields[field].options){
                            FormFields = fields[field].options['fillfields'];
                        }
                    }
                }
                /** when the view is on edit mode init google place api and add the new listener**/
                if (!this.get("effective_readonly")) {
                    this.$txt.val(show_value);
                    this.$el.trigger('resize');
                    placesAutocomplete = new google.maps.places.Autocomplete(document.getElementById(this.name),{types: ['geocode']});
                    placesAutocomplete.addListener('place_changed', fillInAddress);
                } else {
                    this.$(".oe_form_char_content").text(show_value);
                }


                /**This function fill the form fields when the address is clicked**/
                function fillInAddress(){
                    // Get the place details from the autocomplete object.
                    var place = placesAutocomplete.getPlace();
                    var ArrayData = [];
                    var aux = [];
                    if (place != undefined){
                        if ('address_components' in place) {
                            for (var i = 0; i < place.address_components.length; i++) {
                                var addressType = place.address_components[i].types[0];
                                if (componentForm[addressType]) {
                                      var val = place.address_components[i][componentForm[addressType]];
                                      input = selector.find('input[id="' + FormFields[addressType] + '"]');

                                      if (fields[FormFields[addressType]].field.type == 'many2one'){
                                          var relation = fields[FormFields[addressType]].field.relation;
                                          if (relation == "res.country"){
                                            if (val != undefined){
                                            fields[FormFields[addressType]].set_value(Countries[val]);
                                            fields[FormFields[addressType]].trigger('changed_value');
                                            }
                                          }
                                          if (relation == "res.country.state"){
                                            if (val != undefined){
                                            fields[FormFields[addressType]].set_value(States[val]);
                                            fields[FormFields[addressType]].trigger('changed_value');
                                            }
                                          }
                                      }else{
                                          if (aux[0] != FormFields[addressType]){
                                            fields[FormFields[addressType]].set_value(val);
                                            aux = [FormFields[addressType], val];
                                          }else{
                                            fields[FormFields[addressType]].set_value(val+' '+aux[1]);
                                          }
                                          //fields[FormFields[addressType]].trigger('changed_value');
                                      }
                                }
                            }
                        }
                    }
                }/*End FillInAddress*/

            },

            format_value: function (val, def) {
                return oe.web.format_value(val, this, def);
            },

        }
    );

    oe.web.FormView.include({
        to_edit_mode: function () {
            this._super();

        },

    });
};
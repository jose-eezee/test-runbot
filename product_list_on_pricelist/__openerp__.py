# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    
#    Copyright (c) 2013 Eezee-it nv/sa (www.eezee-it.com). All rights reserved.
# 
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Product list on pricelist',
    'version': '0.1',
    'license': 'AGPL-3',
    'author': 'Eezee-it',
    'website': 'http://www.eezee-it.com',
    'category': 'Reporting',
    'description': """ 
    """,
    'depends': ['base', 'product', 'domain_generator'],
    'data': [
        # Views
        "views/pricelist.xml",
        "views/product.xml",

        # Security
        "security/ir.model.access.csv",
    ],
    'demo': [
        'demo/res.partner.csv',
        'demo/product.category.csv',
        'demo/product.product.csv',
        'demo/product.list.csv',
        'demo/product.list.item.csv',
        'demo/product.pricelist.csv',
        'demo/product.pricelist.version.csv',
        'demo/product.pricelist.item.csv',
    ],
    'active': False,
    'installable': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

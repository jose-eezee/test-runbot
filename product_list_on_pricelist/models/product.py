# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-it
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.fields import Many2many, Many2one, Char, One2many
from openerp.models import Model, api, _
from openerp.addons.domain_generator.tools import tools


class ProductProduct(Model):
    _inherit = 'product.product'

    product_list_ids = Many2many('product.list', 'product_list_product_rel', 'product_id', 'list_id',
                                 string='Product Lists', readonly=True)

    @api.model
    def create(self, vals):
        result = super(ProductProduct, self).create(vals)

        result.update_products_list()
        return result

    @api.multi
    def write(self, vals):
        result = super(ProductProduct, self).write(vals)

        self.update_products_list()
        return result

    @api.multi
    def check_product_validity(self, product_list):
        self.ensure_one()

        domain = product_list.get_domains()
        products = self.sudo().search(domain)

        if self[0] not in products:
            return False
        return True

    @api.multi
    def update_products_list(self):
        product_lists = self.env['product.list'].search([])

        for product in self:
            for product_list in product_lists:
                if product.check_product_validity(product_list) and product not in product_list.product_ids:
                    product_list.product_ids += product



class ProductListItem(Model):
    _name = "product.list.item"

    name = Char('Name', required=True)
    product_list_id = Many2one('product.list', required=True, on_delete='cascade')
    domain = Char('Domain')

    @api.multi
    def get_domain(self):
        self.ensure_one()
        return tools.eval_domain_str(self[0].domain)

class ProductList(Model):
    _name = "product.list"

    name = Char('Name', required=True)
    product_list_item_ids = One2many('product.list.item', 'product_list_id', string='Product List Items')
    product_ids = Many2many('product.product', 'product_list_product_rel', 'list_id', 'product_id',
                            string='Products', readonly=True)

    @api.multi
    def get_domains(self):
        self.ensure_one()
        domains_list = [item.get_domain() for item in self[0].product_list_item_ids]
        return tools.merge_domains(domains_list)

    @api.multi
    def update_products(self):
        product_obj = self.env['product.product']

        for list in self:
            full_domain = list.get_domains()
            list.product_ids = product_obj.sudo().search(full_domain)

    @api.constrains('product_list_item_ids')
    @api.multi
    def automatically_update_products(self):
        self.update_products()
from openerp.tests.common import TransactionCase


class TestProductList(TransactionCase):
    def setUp(self):
        super(TestProductList, self).setUp()
        self.product_list_obj = self.env['product.list']
        self.product_product_obj = self.env['product.product']

        self.product_list_obj.search([]).update_products()

    def test_product_in_product_list_1(self):
        """
        Test if the product Apple is in the product list "Apple"
        """
        ipad = self.env.ref('product.product_product_6')
        product_list_apple = self.env.ref('product_list_on_pricelist.products_list_1')

        self.assertIn(ipad.id, [product.id for product in product_list_apple.product_ids],
                      'The product iPad Mini is not in the product list "Apple"')

    def test_product_in_product_list_2(self):
        """
        Test if the product Apple is not in the product list "Samsung"
        """
        ipad = self.env.ref('product.product_product_6')
        product_list_samsung = self.env.ref('product_list_on_pricelist.products_list_2')

        self.assertNotIn(ipad.id, [product.id for product in product_list_samsung.product_ids],
                      'The product iPad Mini cannot be in the product list "Samsung"')

    def test_product_in_product_list_3(self):
        """
        Create a new product "new iPad" with a cust_price greater than 300. This product must be found in the product list "Apple"
        """
        apple_product_category = self.env.ref('product.ipad')

        vals = {'name': 'new iPad',
                'standard_price': 400,
                'categ_id': apple_product_category.id}
        product = self.product_product_obj.create(vals)

        product_list_apple = self.env.ref('product_list_on_pricelist.products_list_1')

        self.assertIn(product.id, [product.id for product in product_list_apple.product_ids],
                      'The product new iPad is not in the product list "Apple"')

    def test_product_in_product_list_4(self):
        """
        Create a new product "new iPad" with a cust_price less than 300. This product cannot be found in the product list "Apple"
        """
        apple_product_category = self.env.ref('product.ipad')

        vals = {'name': 'new iPad',
                'standard_price': 200,
                'categ_id': apple_product_category.id}
        product = self.product_product_obj.create(vals)

        product_list_apple = self.env.ref('product_list_on_pricelist.products_list_1')

        self.assertNotIn(product.id, [product.id for product in product_list_apple.product_ids],
                      'The product new iPad cannot be in the product list "Apple"')

    def test_product_in_product_list_5(self):
        """
        Create a new product "new Samsung Galaxy 6" with a cust_price greater than 300. This product cannot be found in the product list "Apple"
        """
        samsung_product_category = self.env.ref('product.galaxy6')

        vals = {'name': 'new Samsung Galaxy 6',
                'standard_price': 400,
                'categ_id': samsung_product_category.id}
        product = self.product_product_obj.create(vals)

        product_list_apple = self.env.ref('product_list_on_pricelist.products_list_1')

        self.assertNotIn(product.id, [product.id for product in product_list_apple.product_ids],
                      'The product new Samsung Galaxy 6 cannot be in the product list "Apple"')

    def test_product_in_product_list_6(self):
        """
        Create a new product "new Galaxy S6 Edge". This product must be in the list "Samsung"
        """
        samsung_product_category = self.env.ref('product.galaxy6')

        vals = {'name': 'new Galaxy S6 Edge 128 GB',
                'standard_price': 400,
                'categ_id': samsung_product_category.id}
        product = self.product_product_obj.create(vals)

        product_list_samsung = self.env.ref('product_list_on_pricelist.products_list_2')

        self.assertIn(product.id, [product.id for product in product_list_samsung.product_ids],
                      'The product new Galaxy S6 Edge must be in the product list "Samsung"')
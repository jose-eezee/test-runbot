# hide_delele_button
# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Eezee-it nv/sa (www.eezee-it.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Hide delete button',
    'version': '1.0',
    'author': 'Eezee-It',
    'website': 'http://www.eezee-it.com',
    'category': 'Generic Modules/Others',
    'description': """
Hide the button delete on the list view.
========================================
This module allows to hide the button delete on the view list.
This module look if the field "cannot_be_deleted" exist on the current object.
Therefore, if the value is True, the module hide the button""",
    'depends': ['base', 'web'],
    'data' : ['views/list_view.xml'],
    'qweb' : [
        'static/src/xml/list_view.xml',
    ],
    'active': False,
    'installable': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

openerp.hide_delete_button = function(instance) {
    var _t = instance.web._t;
    var QWeb = instance.web.qweb;

    instance.web.ListView.include({
        cannot_be_deleted: function(record) {
            var cannot_be_deleted = record.attributes.cannot_be_deleted

            if(typeof cannot_be_deleted === 'undefined'){
                return false
            }

            return cannot_be_deleted
        }
    });
};
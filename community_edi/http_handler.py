import urllib2

def send_XML(cr, uid, pool, xml_string, realm=None, url=None, user=None, password=None, logger=None):
    auth_handler = urllib2.HTTPBasicAuthHandler()
    auth_handler.add_password(realm=realm,
        uri=url,
        user=user,
        passwd=password)
    opener = urllib2.build_opener(auth_handler)

    try:
        opener.open(url, '<?xml version="1.0" encoding="UTF-8"?>%s' % xml_string)
    except IOError, e:
        if logger:
            logger.error(e)
        raise e
    return True
from openerp.osv import orm, fields
from datetime import datetime
import base64
import logging
import traceback

logger = logging.getLogger(__name__)


class edi_log(orm.Model):
    _name = 'edi_log'
    _order = 'date DESC'

    _type = (
        ('in', 'Incoming'),
        ('out', 'Outgoing')
    )

    _state = (
        ('in_queue_in', 'In queue to be treated'),
        ('importing', 'Importing'),
        ('imported', 'Imported'),
        ('imp_with_err', 'Imported with errors'),
        ('error', 'In error'),
        ('in_queue_out', 'In queue to be sent on the server'),
        ('preview', 'Preview'),
        ('sent', 'Sent on the server')
    )

    def add_xml(self, cr, uid, id, xml_string):
        """
        Add the specified xml_string to the log as a file.
        Encoding it with base64.
        @param cr: cursor
        @param uid: user id
        @param id: id of the log
        @param xml_string: XML string
        @return: True
        """
        self.write(cr, uid, [id], {'xml_string': xml_string})
        if not xml_string:
            return False
        try:
            encoded_xml = base64.encodestring(xml_string)
        except UnicodeEncodeError, e:
            traceback.print_exc()
            encoded_xml = base64.encodestring('An error occured while reading the xml file, check the incoming encoding of the file')
        self.write(cr, uid, [id], {'xml_file': encoded_xml})
        return True

    def log(self, cr, uid, id, error=None, message=None, state=None):
        """
        Convenience method to add a message to the log and/or a state.
        """
        if not message and not state:
            return True
        values = {}
        if message:
            logger.debug(message)
            old_message = self.read(cr, uid, id, ['message']).get('message', False)
            if old_message:
                old_message += '\n' + str(message)
            else:
                old_message = str(message)
            values = {
                'message': old_message
            }
        if error:
            logger.warn(error)
            old_error = self.read(cr, uid, id, ['errors']).get('errors', False)
            if old_error:
                old_error += '\n' + str(error)
            else:
                old_error = str(error)
            values = {
                'errors': old_error
            }
        if state:
            values.update({
                'state': state
            })
        self.write(cr, uid, [id], values)
        return True

    def see_entity(self, cr, uid, ids, context):
        log = self.browse(cr, uid, ids)[0]

        return {'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': log.model_id.model,
                'context': {'init': True},
                'target': 'current',
                'res_id': log.entity_id,
        }

    def retry(self, cr, uid, ids, context=None):
        """
        Retry to import document by launching the edi.introduce.document or edi_send_document wizard,
        with xml and model name as default values
        """
        log = self.browse(cr, uid, ids[0])
        view = False
        if log.type == 'in':
            view = {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'edi.introduce.document',
                'target': 'new',
                'context': {'xml_string': log.xml_string, 'pattern_id': log.pattern_id.id},
            }
        elif log.type == 'out':
            # Get model
            model_id = False
            model_name = log.entity_id and log.entity_id._name or False
            if model_name:
                model_ids = self.pool.get('ir.model').search(cr, uid, [('model', '=', model_name)])
                if model_ids and len(model_ids) == 1:
                    model_id = model_ids[0]
            view = {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'edi_send_document',
                'target': 'new',
                'context': {'pattern_id': log.pattern_id and log.pattern_id.id or False, 'model_id': model_id, 'document_id': log.entity_id and log.entity_id.id or False,
                            'partner_id': log.partner_id and log.partner_id.id or False},
            }
        return view

    def _get_models(self, cr, uid, context=None):
        """
        Return a list of tuples (model.model, model.name) of models having a pattern.
        @param cr: cursor
        @param uid: user id
        @param context: context
        @return: list of tuples (model.model, model.name)
        """
        res = []
        pattern_ids = self.pool.get('edi_pattern_model').search(cr, uid, [])
        if not pattern_ids:
            return res
        model_ids = set([pattern.model_id.id for pattern in self.pool.get('edi_pattern_model').browse(cr, uid, pattern_ids) if pattern.model_id])
        res = self.pool.get('ir.model').read(cr, uid, list(model_ids), ['model', 'name'], context=context)
        return [(r['model'], r['name']) for r in res]

    _columns = {
        'pattern_id': fields.many2one('edi_pattern_model', 'Pattern'),
        'entity_id': fields.reference('Entity', selection=_get_models, size=128),
        'xml_file': fields.binary('XML File'),
        'xml_string': fields.text('XML string'),
        'date': fields.datetime('Date'),
        'type': fields.selection(_type, 'Message type'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'message': fields.text('Message'),
        'errors': fields.text('Errors'),
        'state': fields.selection(_state, 'State'),
        'edi_reference': fields.char('EDI Reference', size=64),
        'strict': fields.boolean('Strict'),
    }

    _defaults = {
        'date': lambda *a: datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
    }
from openerp.osv import orm, fields

class edi_sale_order(orm.BaseModel):
    _inherit = 'sale.order'

    _columns = {
        'edi_reference': fields.char('EDI Reference', size=64),
        }

class edi_sale_order_line(orm.BaseModel):
    _inherit = 'sale.order.line'

    _columns = {
        'edi_reference': fields.related('order_id','edi_reference', type='char', size=64, string="EDI Reference"),
        }
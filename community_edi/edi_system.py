from openerp.osv import orm, fields
from edi_serializer import serializer
import logging

logger = logging.getLogger(__name__)


class edi_system(orm.BaseModel):
    """
    This class is called from edi.gateway and handles the logging, serialization and deserialization through
    edi_serializer, and hooks methods.
    """
    _name = 'edi_system'

    def __init__(self, pool, cr):
        self.dbname = cr.dbname
        super(edi_system, self).__init__(pool, cr)

    def before_serialize(self, cr, uid, model_name, additional_values=None, context=None, **kwargs):
        return {}

    def serialize_before_add_element(self, cr, uid, model_name, xml_xpath=None, value=None, res_id=None, xml=None, pattern_id=None, additional_values=None, **kwargs):
        return value

    def after_serialize_model(self, cr, uid, model_name, xml=None, entity=None, id=None, additional_values=None, **kwargs):
        return xml

    def after_serialize(self, cr, uid, model_name, res_id=None, xml=None, pattern_id=None, context=None, **kwargs):
        return xml

    def serialize(self, cr, uid, pattern_id, document_id, partner_id=None, preview=None, log_cr=None, log_id=None, add_tag_if_empty=True, context=None):
        return serializer(cr, uid, self.pool, pattern_id=pattern_id, log_cr=log_cr, log_id=log_id, add_tag_if_empty=add_tag_if_empty).serialize_object(document_id)

    def before_deserialize(self, cr, uid, model_name, xml=None, context=None, **kwargs):
        return {}

    def before_create_entity(self, cr, uid, model_name, entity=None, additional_values=None, **kwargs):
        return entity

    def deserialize(self, cr, uid, pattern_id, xml, log_id=None, log_cr=None, strict=None, context=None):
        return serializer(cr, uid, self.pool, pattern_id=pattern_id, log_cr=log_cr, log_id=log_id, xml=xml).deserialize_object()

    def after_deserialize_model(self, cr, uid, model_name, entity=None, additional_values_for_model=None, additional_values=None, **kwargs):
        return entity

    def after_create_entity(self, cr, uid, model_name, id, additional_values=None, **kwargs):
        pass

    _columns = {
        'name': fields.char('Name', size=64),
        'sequence': fields.integer('Sequence'),
    }

    _defaults = {
        'name': lambda *a: 'edi_system',
    }

# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import orm, fields
from openerp import pooler
import http_handler
import logging
import traceback
import simplejson

try:
    from lxml import etree

    print("running with lxml.etree")
except ImportError:
    try:
        # Python 2.5
        import xml.etree.cElementTree as etree

        print("running with cElementTree on Python 2.5+")
    except ImportError:
        try:
            # Python 2.5
            import xml.etree.ElementTree as etree

            print("running with ElementTree on Python 2.5+")
        except ImportError:
            try:
                # normal cElementTree install
                import cElementTree as etree

                print("running with cElementTree")
            except ImportError:
                try:
                    # normal ElementTree install
                    import elementtree.ElementTree as etree

                    print("running with ElementTree")
                except ImportError:
                    print("Failed to import ElementTree from any known place")

logger = logging.getLogger(__name__)


class edi_gateway(orm.Model):
    """
    This class exposes the deserialize and send_document methods.
    The deserialize method will be called from EDI suppliers (ie Babelway)
    The send_document will be called from OpenERP and will send documents to EDI suppliers.
    """
    _name = 'edi.gateway'

    _columns = {
    }

    def _get_response(self, cr, uid, log_cr=None, log_id=None, context=None):
        log_pool = self.pool.get('edi_log')

        # Prepare response
        response_root = etree.Element("response")
        statusEl = etree.SubElement(response_root, "status")
        statusEl.text = "0"

        errors = log_pool.browse(log_cr, uid, log_id).message or ""
        if errors and len(errors) > 0:
            error_lines = errors.split("\n")
            if error_lines:
                statusEl.text = "err"
                errorsEl = etree.SubElement(response_root, "errors")
                for error_line in error_lines:
                    errorEl = etree.SubElement(errorsEl, "error")
                    errorEl.text = error_line

        log_cr.close()
        return etree.tostring(response_root, pretty_print=True)

    def _get_success_response(self, cr, uid, context=None):
        # Prepare response
        response_root = etree.Element("response")
        statusEl = etree.SubElement(response_root, "status")
        statusEl.text = "0"
        return etree.tostring(response_root, pretty_print=True)

    def deserialize(self, cr, uid, xml, model_name=None, pattern_name=None, strict=None, context=None):
        """
        Deserialize an xml based on the pattern name passed as parameter.
        If not pattern name, try to get the pattern for the specified model name.
        The method will create an edi_log containing the informations needed to deserialize.
        A cron job will then check all the waiting documents and deserialize them.
        @param cr: the cursor
        @param uid: the user id
        @param xml: the xml to deserialize
        @param model_name: the name of the model (ie sale.order)
        @param pattern_name: the name of the edi system to use (ie edi_babelway)
        @param strict: strict mode stops the import at the first encountered error
        @param context: the context
        @return: Returns an XML response which XSD is :
        <?xml version="1.0" encoding="utf-16"?>
        <xsd:schema attributeFormDefault="unqualified" elementFormDefault="qualified" version="1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
          <xsd:element name="response">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="status" type="xsd:string" />
                <xsd:element name="errors">
                  <xsd:complexType>
                    <xsd:sequence>
                      <xsd:element maxOccurs="unbounded" name="error" type="xsd:string" />
                    </xsd:sequence>
                  </xsd:complexType>
                </xsd:element>
              </xsd:sequence>
            </xsd:complexType>
          </xsd:element>
        </xsd:schema>

        Status = 0 if all ok
        """
        logger.info("Deserialize called for model %s / pattern %s / strict %s" % (model_name, pattern_name, strict))
        log_pool = self.pool.get('edi_log')

        # Put the XML in the queue
        log_id = log_pool.create(cr, uid, {'xml_string': xml, 'type': 'in', 'state': 'in_queue_in', 'strict': strict})
        log_pool.add_xml(cr, uid, log_id, xml)  # Convenience method that will convert XML string to file

        if not model_name and not pattern_name:
            log_pool.log(cr, uid, log_id, "No model and no pattern name defined, cannot deserialize.", state="error")
            return self._get_response(cr, uid, log_cr=cr, log_id=log_id, context=context)

        pattern_id = False

        if pattern_name:
            pattern_ids = self.pool.get('edi_pattern_model').search(cr, uid, [('name', '=', pattern_name)])
            if not pattern_ids or len(pattern_ids) == 0:
                log_pool.log(cr, uid, log_id, "Invalid pattern name defined (%s)." % pattern_name, state="error")
            elif len(pattern_ids) > 1:
                log_pool.log(cr, uid, log_id, "Cannot find unique pattern with name %s." % pattern_name, state="error")
            else:
                pattern_id = pattern_ids[0]
                log_pool.write(cr, uid, [log_id], {'pattern_id': pattern_id})

        # Pattern not yet found and no model_name
        if not pattern_id and not model_name:
            log_pool.log(cr, uid, log_id, "No model name, cannot deserialize.", state="error")
            return self._get_response(cr, uid, log_cr=cr, log_id=log_id, context=context)

        # Try to get the pattern corresponding to the model specified
        if not pattern_id and model_name:
            model_ids = self.pool.get('ir.model').search(cr, uid, [('model', '=', model_name)])
            if not model_ids or len(model_ids) != 1:
                log_pool.log(cr, uid, log_id, "Invalid model defined (%s), cannot deserialize." % model_name, state="error")
                return self._get_response(cr, uid, log_cr=cr, log_id=log_id, context=context)
            model_id = model_ids[0]
            # Try to get corresponding pattern
            pattern_ids = self.pool.get('edi_pattern_model').search(cr, uid, [('model_id', '=', model_id)])
            if not pattern_ids:
                log_pool.log(cr, uid, log_id, "No pattern found for model %s, cannot deserialize." % model_name, state="error")
                return self._get_response(cr, uid, log_cr=cr, log_id=log_id, context=context)
            elif len(pattern_ids) > 1:
                log_pool.log(cr, uid, log_id, "Multiple patterns found for model %s, cannot deserialize." % model_name, state="error")
                return self._get_response(cr, uid, log_cr=cr, log_id=log_id, context=context)
            else:
                pattern_id = pattern_ids[0]
                log_pool.write(cr, uid, [log_id], {'pattern_id': pattern_id})

        # Check pattern
        if not pattern_id:
            log_pool.log(cr, uid, log_id, "Could not find a pattern, cannot deserialize.", state="error")
            return self._get_response(cr, uid, log_cr=cr, log_id=log_id, context=context)

        return self._get_success_response(cr, uid, context=context)

    def send_document(self, cr, uid, pattern_id, document_id, partner_id=None, preview=None, context=None):
        """
        Send the specified document with the specified pattern.
        The document will be put in the queue to be sent by a cron.
        @param cr: cursor
        @param uid: user id
        @param pattern_id: pattern
        @param document_id: id of the document
        @param partner_id: partner of the document (optional, will be shown in logs)
        @param preview: if preview, returns directly the XML
        @param context: context
        @return: True
        """
        log_pool = self.pool.get('edi_log')

        if not document_id:
            log_pool.create(cr, uid, {'type': 'out', 'state': 'error', 'partner_id': partner_id, 'message': 'No document defined.'})
            return False

        if not pattern_id:
            log_pool.create(cr, uid, {'type': 'out', 'state': 'error', 'partner_id': partner_id, 'message': 'No pattern defined for document %s' % document_id})
            return False

        pattern = self.pool.get('edi_pattern_model').browse(cr, uid, pattern_id)
        if not pattern or not pattern.exists():
            log_pool.create(cr, uid, {'type': 'out', 'state': 'error', 'partner_id': partner_id, 'message': 'This pattern id %s does not exist' % pattern_id})
            return False

        model = pattern.model_id

        if preview:
            # Get cursor only for logging
            db, pool = pooler.get_db_and_pool(cr.dbname)
            log_cr = db.cursor(serialized=False)
            log_cr.autocommit(True)

            # Call serialize synchronously and directly returns the result
            log_id = log_pool.create(log_cr, uid,
                                     {'type': 'out', 'state': 'preview', 'partner_id': partner_id, 'pattern_id': pattern_id, 'entity_id': '%s,%s' % (model.model, document_id)})
            try:
                res = self.pool.get('edi_system').serialize(cr, uid, pattern_id, document_id, partner_id=partner_id, preview=preview, log_cr=log_cr, log_id=log_id, context=context)
            except Exception, e:
                logger.exception(e)
                return False
            finally:
                log_cr.close()
            return res

        else:
            # Put the document in the queue to be sent
            log_pool.create(cr, uid,
                            {'type': 'out', 'state': 'in_queue_out', 'partner_id': partner_id, 'entity_id': '%s,%s' % (model.model, document_id), 'pattern_id': pattern_id})
        return True

    def _send_XML(self, cr, uid, xml_string, pattern_id=None):
        """
        Send XML with a HTTP handler.
        @param cr: cursor
        @param uid: user id
        @param xml_string: string representing the XML
        @param pattern_id: pattern
        @return: True
        """
        if not pattern_id:
            raise orm.except_orm('Error', 'No pattern defined, cannot send')

        pattern = self.pool.get('edi_pattern_model').browse(cr, uid, pattern_id)
        json_pattern = simplejson.loads(pattern.json_pattern)
        if not json_pattern:
            return False
        user = json_pattern.get('login', False)
        password = json_pattern.get('password', False)
        url = json_pattern.get('url', False)
        realm = json_pattern.get('realm', False)
        # TODO : if no user / password, send with basic http handler, rename this one in
        # auth http handler?
        if not url:
            raise Exception("Cannot send document without URL")
        http_handler.send_XML(cr, uid, self.pool, xml_string, realm, url, user, password)
        return True

    def edi_send_receive_cron(self, cr, uid, context=None):
        """
        This method will be called by the cron.
        It will check if there are documents in the queue (edi_log) ready to be deserialized,
        then check if there are documents in the queue ready to be sent.
        @param cr: cursor
        @param uid: user id
        @param context: context
        @return: True
        """
        log_pool = self.pool.get('edi_log')

        logger.debug("Treating EDI receive queue (edi_send_receive_cron)")
        # Get logs where state is importing and treat them
        log_ids = log_pool.search(cr, uid, [('state', '=', 'in_queue_in')])
        if not log_ids:
            logger.debug("No EDI document waiting to be received")
        else:
            logger.debug("%s EDI documents to treat" % len(log_ids))
            for log in log_pool.browse(cr, uid, log_ids):
                logger.debug("Treating EDI log %s" % log.id)
                db, pool = pooler.get_db_and_pool(cr.dbname)

                # Get a cursor for import
                import_cr = db.cursor()

                # Get a cursor only for logging, for each log
                log_cr = db.cursor()
                log_cr.autocommit(True)
                xml = log.xml_string
                strict = log.strict or False
                pattern_id = log.pattern_id and log.pattern_id.id or False
                try:
                    self.pool.get('edi_system').deserialize(import_cr, uid, pattern_id, xml, log_cr=log_cr, log_id=log.id, strict=strict, context=context)
                    import_cr.commit()
                except Exception, e:
                    traceback.print_exc()
                    log_pool.log(log_cr, uid, log.id, e, state="error")
                    import_cr.rollback()
                finally:
                    import_cr.close()
                    log_cr.close()

        logger.debug("Treating EDI send queue (edi_send_receive_cron)")
        # Get logs ready to be sent and treat them
        log_ids = log_pool.search(cr, uid, [('state', '=', 'in_queue_out')])
        if not log_ids:
            logger.debug("No EDI document waiting to be sent")
        else:
            logger.debug("%s EDI documents to treat" % len(log_ids))

            # We need another cursor for the thread, because our cr will be closed
            for log in log_pool.browse(cr, uid, log_ids):
                logger.debug("Treating EDI log %s" % log.id)
                db, pool = pooler.get_db_and_pool(cr.dbname)

                # Get a cursor for import
                import_cr = db.cursor()

                # Get a log only for logging, for each log
                log_cr = db.cursor()
                log_cr.autocommit(True)
                partner_id = log.partner_id and log.partner_id.id or False
                pattern_id = log.pattern_id and log.pattern_id.id or False
                try:
                    # Deserialize with edi_system
                    xml_string = self.pool.get('edi_system').serialize(import_cr, uid, pattern_id, log.entity_id and log.entity_id.id or False, partner_id=partner_id or False,
                                                                       log_cr=log_cr, log_id=log.id, context=context)
                    if not xml_string:
                        log_pool.log(log_cr, uid, log.id, message="No XML or XML empty.", state="error")
                        continue
                    log_pool.add_xml(log_cr, uid, log.id, xml_string)
                    # Send
                    self._send_XML(cr, uid, xml_string, pattern_id=pattern_id)
                    log_pool.log(log_cr, uid, log.id, state="sent")
                    import_cr.commit()
                except Exception, e:
                    traceback.print_exc()
                    log_pool.log(log_cr, uid, log.id, e, state="error")
                    import_cr.rollback()
                finally:
                    import_cr.close()
                    log_cr.close()
        return True
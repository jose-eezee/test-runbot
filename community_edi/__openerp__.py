# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    "name": "Taktik EDI Connector",
    "version": "1.1",
    "author": "Taktik S.A.",
    "category": "Tools",
    "website": "http://www.taktik.be",
    "description": """
Taktik EDI Connector
=====================

This module offers you a generic way to transfer documents to your partners through a EDI company.
You will be able to send and receive documents using the XML format.
It allows to create patterns for specific models from Odoo, defining the structure of your XML.

Security
========
Users in employee group will benefit from the automatic edi sending.
Users in EDI group will have access to the EDI Connector configuration.
""",
    "depends": ['base', 'sale'],
    "init_xml": [
    ],
    "demo_xml": [],
    "data": [
        'security/edi_security.xml',
        'data/edi_cron.xml',
        'view/pattern_model_view.xml',
        'view/edi_system_view.xml',
        'view/edi_product_view.xml',
        'view/edi_partner_view.xml',
        'view/edi_sale_view.xml',
        'view/edi_log_view.xml',
        'menu/edi_menu.xml',
        'wizard/edi_send_document_wizard.xml',
        'wizard/edi_introduce_document_wizard_view.xml',
        'data/edi_system.xml',
        'security/ir.model.access.csv',
    ],
    "active": False,
    "installable": True,
    "application": True,
}
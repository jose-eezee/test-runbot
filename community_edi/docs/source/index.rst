.. Taktik EDI documentation master file, created by
   sphinx-quickstart on Tue Oct  1 22:53:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##########
Taktik EDI
##########

This module allows you to send and receive XML files.

.. _Taktik: http://www.taktik.be

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



*****************
Developer's guide
*****************

.. toctree::
   :maxdepth: 2

API Reference
=============

.. toctree::
   :maxdepth: 1

   api/api_serializer.rst
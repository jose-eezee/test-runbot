{
    "name": "Sale Order Pattern",
    "url": "http://192.168.101.100:8069/",
    "realm": "realm",
    "login": "admin",
    "password": "admin",
    "model": "sale.order",
    "xpath": "Doc",
    "date_format": "format",
    "date_format_sent": "format",
    "fields": [
        {

            "xpath": "Doc/date"
        },
        {
            "multi": "True",
            "xpath": "Doc/SO",
            "fields": [
                {
                    "name": "name",
                    "xpath": "Doc/SO/order_name"
                },
                {
                    "name": "edi_reference",
                    "xpath": "Doc/SO/edi_ref"
                },
                {
                    "name": "partner_id",
                    "xpath": "Doc/SO/partner",
                    "fields": [
                        {
                            "name":"name",
                            "xpath":"Doc/SO/partner/name"
                        },
                        {
                            "name":"ean13",
                            "xpath":"Doc/SO/partner/ean_code"
                        },
                        {
                            "name":"street",
                            "xpath":"Doc/SO/partner/address1"
                        },
                        {
                            "name":"street2",
                            "xpath":"Doc/SO/partner/address2"
                        },
                        {
                            "name":"city",
                            "xpath":"Doc/SO/partner/city"
                        },
                        {
                            "name":"zip",
                            "xpath":"Doc/SO/partner/zip_code"
                        },
                        {
                            "name": "country_id",
                            "xpath": "Doc/SO/partner/country",
                            "fields": [
                                {
                                    "name":"code",
                                    "xpath":"Doc/SO/partner/country/code"
                                }
                            ]
                        }
                    ]
                },
                {
                    "name": "date_order",
                    "xpath": "Doc/SO/date_order"
                },
                {
                    "name": "requested_date",
                    "xpath": "Doc/SO/requested_date"
                },
                {
                    "name": "client_order_ref",
                    "xpath": "Doc/SO/client_order_ref"
                },
                {
                    "name": "pricelist_id",
                    "xpath": "Doc/SO/pricelist",
                    "fields": [
                        {
                            "name":"name",
                            "xpath":"Doc/SO/pricelist"
                        }
                    ]
                },
                {
                    "name": "amount_total",
                    "xpath": "Doc/SO/amount_total"
                },
                {
                    "name":"currency_id",
                    "xpath":"Doc/SO/currency",
                    "fields": [
                        {
                            "name":"name",
                            "xpath":"Doc/SO/currency"
                        }
                    ]
                },
                {
                    "name": "user_id",
                    "xpath": "Doc/SO/user_name",
                    "fields": [
                        {
                            "name":"login",
                            "xpath":"Doc/SO/user_name"
                        }
                    ]
                },
                {
                    "name": "order_line",
                    "xpath": "Doc/SO/lines/line",
                    "fields": [
                        {
                            "name": "product_id",
                            "xpath": "Doc/SO/lines/line/product",
                            "fields": [
                                {
                                    "name": "ean13",
                                    "xpath": "Doc/SO/lines/line/product/ean13"
                                }
                            ]
                        },
                        {
                            "name": "name",
                            "xpath": "Doc/SO/lines/line/description"
                        },
                        {
                            "name": "product_uom_qty",
                            "xpath": "Doc/SO/lines/line/quantity"
                        },
                        {
                            "name": "price_unit",
                            "xpath": "Doc/SO/lines/line/price_unit"
                        }
                    ]
                }
            ]
        }
    ]
}
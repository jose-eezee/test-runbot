"""
:The login function is under
::    http://localhost:8069/xmlrpc/common
:For object retrieval use:
::    http://localhost:8069/xmlrpc/object
"""
import xmlrpclib

user = 'admin'
pwd = 'admin'
dbname = 'chacon_clean_2014_06_11'
model = 'edi.gateway'
file_to_read = 'import_sale_order.xml'
ide_model_to_create = 'sale.order'

sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/common')
uid = sock.login(dbname ,user ,pwd)

sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')

with open ("import_sale_order.xml", "r") as myfile:
    xml_content=myfile.read()


print sock.execute(dbname, uid, pwd, model, 'deserialize', xml_content, ide_model_to_create)
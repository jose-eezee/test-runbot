from openerp.osv import orm, fields
from openerp import SUPERUSER_ID
from urlparse import  urljoin
from urllib import urlencode


class edi_send_document(orm.TransientModel):
    """
    This wizard allows the sending of a document through EDI.
    You can call it with an act window and set default values in the context (pattern_id, partner_id and document_id).
    You can either really send the document (it will be put in the waiting queue) or just preview it.
    """
    _name = "edi_send_document"

    _views = {
        'default': 'edi.send.document.form.view',
        'success': 'edi.send.document.success.form.view',
        'preview': 'edi.send.document.preview.form.view',
    }

    def get_view_dict(self, cr, uid, ids, view_name, context=None):
        message_view_id = self.pool.get('ir.ui.view').search(cr, uid,
                                                             [('name', '=', view_name)])
        result_message = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': self._name,
            'type': 'ir.actions.act_window',
            'context': context,
            'view_id': message_view_id,
            'target': 'new',
            'res_id': ids[0],
        }
        return result_message

    def default_get(self, cr, uid, fields_list, context=None):
        """
        Get the default values based on values passed in the context.
        @param cr: cursor
        @param uid: user id
        @param fields_list: field list
        @param context: context
        @return: dictionary containing for each field its default value
        """
        res = {}
        pattern_id = context.get('pattern_id', False)
        res.update({'pattern_id': pattern_id})

        partner_id = context.get('partner_id', False)
        res.update({'partner_id': partner_id})

        active_id = context.get('document_id', context.get('active_id', False))
        res.update({'document_id': active_id})

        active_model = context.get('model', context.get('active_model', False))

        if active_id and not partner_id:
            document = self.pool.get(active_model).browse(cr, uid, active_id)

            # Guess the partner based on the partner_field defined
            partner_field = context.get('partner_field', False)
            if partner_field:
                partner = getattr(document, partner_field)
                partner_id = partner.id
                res.update({'partner_id': partner_id})

        if partner_id and not pattern_id:
            pattern_id = self.pool.get('res.partner').get_pattern_id(cr, uid, partner_id, active_model)
            res.update({'pattern_id': pattern_id})

        # Try to find a pattern for the model
        if not pattern_id:
            model_ids = self.pool.get('ir.model').search(cr, uid, [('model', '=', active_model)]) if active_model else False
            if model_ids and len(model_ids) == 1:
                pattern_ids = self.pool.get('edi_pattern_model').search(cr, uid, [('model_id', '=', model_ids[0])])
                if pattern_ids and len(pattern_ids) == 1:
                    pattern_id = pattern_ids[0]
                    res.update({'pattern_id': pattern_id})

        return res

    def next(self, cr, uid, ids, context=None, preview=None):
        wizard = self.browse(cr, uid, ids[0])
        document_id = wizard.document_id
        partner_id = wizard.partner_id and wizard.partner_id.id or False
        pattern_id = wizard.pattern_id

        if not pattern_id or not document_id:
            return False

        # Call send_document
        res = self.pool.get('edi.gateway').send_document(cr, uid, pattern_id.id, document_id, partner_id=partner_id, preview=preview, context=None)
        if preview:
            self.write(cr, uid, [wizard.id], {'message': res})
            return self.get_view_dict(cr, uid, ids, self._views['preview'], context=context)

        self.write(cr, uid, [wizard.id], {'message': "The document has been put in the queue to be sent. Please check the logs for more informations."})
        return self.get_view_dict(cr, uid, ids, self._views['success'], context=context)

    def preview(self, cr, uid, ids, context=None):
        return self.next(cr, uid, ids, context=context, preview=True)

    def display_dynamic_values(self, cr, uid, ids, pattern_id, document_id, context=None):
        """
        Get the document display name for the model from pattern with an hyperlink to show the related document
        :param cr: Cursor Database
        :param uid: User ID
        :param ids: Wizard Id's
        :param pattern_id: edi_pattern ID
        :param document_id: Related document ID
        :param context: Contextual data's
        :return: A dictionary with a new value dictionary
        """
        config_param_obj = self.pool.get('ir.config_parameter')
        pattern_obj = self.pool.get('edi_pattern_model')
        model_display_name = ''
        if pattern_id and document_id:
            ir_model_id = pattern_obj.browse(cr, uid, pattern_id, context=context).model_id or False
            model_obj = ir_model_id and ir_model_id.model and self.pool.get(ir_model_id.model) or False
            model_id_search = model_obj.search(cr, uid, [('id', '=', document_id)], context=context)
            model_name_get = model_obj and model_id_search \
                             and model_obj.name_get(cr, uid, [document_id], context=context) or False
            model_display_name = model_name_get and '(%s)' % model_name_get[0][1] or ''

            ## If we have a model object, we can create an hyperlink to the form view of this object
            if ir_model_id and ir_model_id.model and model_display_name:
                base_url = config_param_obj.get_param(cr, SUPERUSER_ID, 'web.base.url')
                query = {'db': cr.dbname}
                fragment = {'id': document_id, 'view_type': 'form', 'model':ir_model_id.model, 'active_id': document_id}
                model_display_name = '<a href="' + \
                                     urljoin(base_url, "/web?%s#%s"
                                                          % (urlencode(query), urlencode(fragment))) + \
                                     '" target="_blank">' + model_display_name  + '</a>'

        return {
            'value': {
                'document_id_display_name': model_display_name
            }
        }

    _columns = {
        'pattern_id': fields.many2one('edi_pattern_model', 'Pattern'),
        'document_id': fields.integer('Document ID'),
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'document_id_display_name': fields.char('Document Name'),
        'message': fields.text('Message'),
    }
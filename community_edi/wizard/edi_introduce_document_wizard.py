from openerp.osv import orm, fields


class edi_introduce_document(orm.TransientModel):
    _name = "edi.introduce.document"

    _views = {
        'default': 'edi.introduce.document.form.view',
        'success': 'edi.introduce.document.success.form.view',
    }

    def get_view_dict(self, cr, uid, ids, view_name, context=None):
        message_view_id = self.pool.get('ir.ui.view').search(cr, uid,
                                                             [('name', '=', view_name)])
        result_message = {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': self._name,
            'type': 'ir.actions.act_window',
            'context': context,
            'view_id': message_view_id,
            'target': 'new',
            'res_id': ids[0],
        }
        return result_message

    def next(self, cr, uid, ids, context=None):
        wizard = self.browse(cr, uid, ids[0])
        self.pool.get('edi.gateway').deserialize(cr, uid, wizard.xml_string, wizard.pattern_id.model_id.model, wizard.pattern_id.name)
        self.write(cr, uid, [wizard.id], {'message': "The document will now be processed. Check the logs for more informations."})
        return self.get_view_dict(cr, uid, ids, self._views['success'], context=context)

    _columns = {
        'xml_string': fields.text('XML'),
        'pattern_id': fields.many2one('edi_pattern_model', 'Pattern', required=True),
        'message': fields.text('Message'),
    }

    _defaults = {
        'pattern_id': lambda self, cr, uid, context=None: context.get('pattern_id', False) if context else None,
        'xml_string': lambda self, cr, uid, context=None: context.get('xml_string') if context else None,
    }
from openerp.osv import orm, fields

class edi_product(orm.BaseModel):
    _inherit = 'product.product'

    _columns = {
        'edi_code': fields.char('EDI Code', size=64),
        }
from openerp.osv import orm, fields
from lxml import etree
import logging

logger = logging.getLogger(__name__)


class edi_pattern_partner_relation(orm.BaseModel):
    """
    This relation saves, for a partner and a model, the desired pattern.
    """
    _name = "edi_partner_pattern_rel"

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner'),
        'model_id': fields.many2one('ir.model', 'Model'),
        'edi_pattern_model_id': fields.many2one('edi_pattern_model', 'EDI Pattern'),
    }


class edi_partner(orm.BaseModel):
    _inherit = 'res.partner'

    def get_pattern_id(self, cr, uid, partner_id, model_name, context=None):
        # Get model id
        model_ids = self.pool.get('ir.model').search(cr, uid, [('model', '=', model_name)])
        if model_ids and len(model_ids) == 1:
            model_id = model_ids[0]
        else:
            raise orm.except_orm("Error", "Cannot find model %s" % model_name)

        # Get pattern for specified partner and model
        rel_ids = self.pool.get('edi_partner_pattern_rel').search(cr, uid, [('partner_id', '=', partner_id), ('model_id', '=', model_id)])
        if not rel_ids or len(rel_ids) <= 0:
            # Search on parent
            partner = self.browse(cr, uid, partner_id)
            if partner and partner.parent_id:
                return self.get_pattern_id(cr, uid, partner.parent_id.id, model_name, context=context)

            return False
        return self.pool.get('edi_partner_pattern_rel').browse(cr, uid, rel_ids[0]).edi_pattern_model_id.id

    def _create_or_update_partner_patterns(self, cr, uid, ids, values, context=None):
        """
        Add or modify entries in edi_partner_pattern_rel relation.
        For each partner, each model can have a different edi system.
        """
        edi_fields = [x for x in values if 'edi_' in x]
        for edi_field in edi_fields:
            pattern_id = values.get(edi_field, False)
            if not pattern_id:
                continue
            model_id = edi_field[4:]  # Remove edi_
            try:
                model_id = int(model_id)
            except Exception, e:
                logger.error("Cannot convert model_id in _create_or_update_partner_patterns")
                logger.error(e)
            if not model_id:
                logger.debug("Cannot find model %s" % model_id)
                continue
            rel_ids = self.pool.get('edi_partner_pattern_rel').search(cr, uid, [('partner_id', '=', ids[0]), ('model_id', '=', model_id)])
            if not rel_ids or len(rel_ids) <= 0:
                self.pool.get('edi_partner_pattern_rel').create(cr, uid, {
                    'partner_id': ids[0],
                    'model_id': model_id,
                    'edi_pattern_model_id': pattern_id,
                })
            else:
                self.pool.get('edi_partner_pattern_rel').write(cr, uid, rel_ids, {
                    'edi_pattern_model_id': pattern_id,
                })
        return True

    def on_change_edi_field(self, cr, uid, ids, edi_pattern_model_id, model_id):
        """
        When edi_ fields change, we directly save their values.
        @param cr: cursor
        @param uid: user id
        @param ids: ids of the current model
        @param edi_pattern_model_id: the id of the edi pattern to use for this partner for the specified model
        @param model_id: the model
        @return: the edi_pattern_model_id as value for the field
        """
        res = {}
        logger.debug("Setting edi_pattern_model_id %s for model %s on partner %s" % (edi_pattern_model_id, model_id, ids))
        field = "edi_%s" % model_id
        self._create_or_update_partner_patterns(cr, uid, ids, {field: edi_pattern_model_id})
        res.setdefault('value', {}).setdefault(field, edi_pattern_model_id)
        return res

    def on_change_edi_on_children(self, cr, uid, ids, edi_on_children):
        """
        When edi_on_children field change, we set use_edi on children of this partner.
        @param cr: cursor
        @param uid: user id
        @param ids: ids of the current model
        @param edi_on_children: true or false
        @return: the value for edi_on_children (true if correctly set on all children)
        """
        res = {}
        partner_ids = []
        errors = []

        if not isinstance(ids, list):
            ids = [ids]

        def get_children(partner):
            for child in partner.child_ids:
                partner_ids.append(child.id)
                get_children(child)

        for partner in self.browse(cr, uid, ids):
            get_children(partner)
            logger.debug("Setting use_edi=%s on child of %s (%s)" % (edi_on_children, ids, partner_ids))
            for partner_id in partner_ids:
                # One at a time, otherwise any error will rollback
                try:
                    self.write(cr, uid, [partner_id], {'use_edi': edi_on_children})
                except Exception, e:
                    logger.error("Exception in on_change_edi_on_children")
                    logger.exception(e)
                    errors.append("Cannot set use_edi on partner %s : %s" % (partner_id, e))
        res.setdefault('value', {}).setdefault('edi_on_children', edi_on_children)
        if errors:
            res.setdefault('warning', {'title': 'Error', 'message': '\n'.join(errors)})
        return res

    def read(self, cr, uid, ids, fields=None, context=None, load='_classic_read'):
        """
        Fill the dynamic fields relative to edi_system.
        For each partner, get the edi_partner_pattern_rel, in which we have for each model the edi system choosed.
        Then add it in the dictionnary.
        """
        vals = super(edi_partner, self).read(cr, uid, ids, fields=fields, context=context, load=load)
        for vals_dict in vals:
            if not isinstance(vals_dict, dict):
                continue
            partner_id = vals_dict.get('id', False)
            rel_ids = self.pool.get('edi_partner_pattern_rel').search(cr, uid, [('partner_id', '=', partner_id)])
            for rel in self.pool.get('edi_partner_pattern_rel').browse(cr, uid, rel_ids):
                model = self.pool.get('ir.model').browse(cr, uid, rel.model_id.id)
                edi_model_name = 'edi_%s' % model.id
                vals_dict[edi_model_name] = rel.edi_pattern_model_id.id
        return vals

    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        res = super(edi_partner, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            # Get the arch
            doc = etree.XML(res['arch'])

            # Find or create EDI page (normally created in edi_partner_view)
            edi_page_el = doc.xpath("//page[@string='EDI']")
            if not edi_page_el or len(edi_page_el) <= 0:
                # Add a page
                edi_page = """
                <page string="EDI">
                </page>
                """
                edi_page_el = etree.XML(edi_page)
                notebook_el = doc.xpath("//notebook")[0]
                notebook_el.append(edi_page_el)
            else:
                edi_page_el = edi_page_el[0]

            # Add group in page
            edi_group_el = etree.SubElement(edi_page_el, "group")

            # Get the available models, for which a pattern exists
            models = set()
            pattern_model_ids = self.pool.get('edi_pattern_model').search(cr, uid, [])
            for pattern_model in self.pool.get('edi_pattern_model').browse(cr, uid, pattern_model_ids):
                if pattern_model.model_id and pattern_model.model_id.id:
                    models.add(pattern_model.model_id.id)

            for model in self.pool.get('ir.model').browse(cr, uid, list(models)):
                # Prepare domain
                available_pattern_models = self.pool.get('edi_pattern_model').search(cr, uid, [('model_id', '=', model.id)])
                res.get('fields', {}).update({
                    'edi_' + str(model.id): {'context': {},
                                             'domain': [('id', 'in', available_pattern_models)],
                                             'relation': 'edi_pattern_model',
                                             'selectable': True,
                                             'string': model.name,
                                             'type': 'many2one',
                                             'views': {}},
                })
                edi_field = etree.SubElement(edi_group_el, "field")
                edi_field.set("name", 'edi_' + str(model.id))
                edi_field.set("on_change", "on_change_edi_field(edi_%s, '%s')" % (model.id, model.id))
            res['arch'] = etree.tostring(doc)
        return res

    _columns = {
        'edi_pattern_ids': fields.one2many('edi_partner_pattern_rel', 'partner_id', 'EDI Patterns'),
        'edi_code': fields.char('EDI Code', size=128),
        'use_edi': fields.boolean('Use EDI'),
        'edi_on_children': fields.boolean('Use EDI on children', help="By selecting this option, all children will automatically have the Use EDI field checked."),
        'parent_ean_on_children': fields.boolean('Use this EAN on children', help="By selecting this option, when sending documents for the children, they will have this EAN instead of theirs.")
    }
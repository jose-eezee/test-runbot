from openerp.tests.common import TransactionCase
from openerp.tools import mute_logger

JSON_PATTERN = """
{
    "name": "SO Pattern",
    "url": "http://...",
    "realm": "realm",
    "login": "taktikhttp",
    "password": "test",
    "model": "sale.order",
    "xpath": "Doc",
    "date_format": "format",
    "date_format_sent": "format",
    "fields": [
        {
            "xpath": "Doc/date"
        },
        {
            "multi": "True",
            "xpath": "Doc/SO",
            "fields": [
                {
                    "name": "amount_total",
                    "xpath": "Doc/SO/amount_total"
                },
                {
                    "name": "user_id",
                    "xpath": "Doc/SO/user_name",
                    "fields": [
                        {
                            "name":"login",
                            "xpath":"Doc/SO/user_name"
                        }
                    ]
                },
                {
                    "name": "order_line",
                    "xpath": "Doc/SO/lines/line",
                    "fields": [
                        {
                            "name": "price_unit",
                            "xpath": "Doc/SO/lines/line/price_unit"
                        }
                    ]
                }
            ]
        }
    ]
}
"""

JSON_PATTERN_SERIALIZE = """
{
    "name": "SO Pattern",
    "url": "http://...",
    "realm": "realm",
    "login": "taktikhttp",
    "password": "test",
    "model": "sale.order",
    "xpath": "Doc",
    "date_format": "format",
    "date_format_sent": "format",
    "fields": [
        {
            "xpath": "Doc/date"
        },
        {
            "multi": "True",
            "xpath": "Doc/SO",
            "fields": [
                {
                    "name": "amount_total",
                    "xpath": "Doc/SO/amount_total"
                },
                {
                    "name": "user_id",
                    "xpath": "Doc/SO/userName",
                    "fields": [
                        {
                            "name": "login",
                            "xpath": "Doc/SO/userName"
                        }
                    ]
                },
                {
                    "name": "order_line",
                    "xpath": "Doc/SO/lines/line",
                    "fields": [
                        {
                            "name": "price_unit",
                            "xpath": "Doc/SO/lines/line/price_unit"
                        }
                    ]
                }
            ]
        }
    ]
}
"""

XML_SO = """<Doc>
<SO>
    <amount_total>10</amount_total>
    <user_name>admin</user_name>
    <lines>
        <line>
            <price_unit>5</price_unit>
        </line>
        <line>
            <price_unit>15</price_unit>
        </line>
        <line>
            <price_unit>25</price_unit>
        </line>
    </lines>
</SO>
</Doc>
"""


class TestDeserialize(TransactionCase):
    def setUp(self):
        super(TestDeserialize, self).setUp()
        self.pattern_model = self.registry('edi_pattern_model')
        self.edi_system = self.registry('edi_system')

    def test_valid_json(self):
        cr, uid = self.cr, self.uid
        so_model_id = self.registry('ir.model').search(cr, uid, [('model', '=', 'sale.order')])[0]
        pattern_id = self.pattern_model.create(cr, uid, {'name': 'Simple SO Pattern',
                                                         'model_id': so_model_id,
                                                         'json_pattern': JSON_PATTERN})

        self.assertTrue(self.pattern_model.validate_json(cr, uid, pattern_id))

    @mute_logger('openerp.addons.community_edi.edi_log')
    def test_deserialize(self):
        cr, uid = self.cr, self.uid

        final_dict = [{}]
        correct_dict = {u'order_line': [{u'price_unit': 5.0}, {u'price_unit': 15.0}, {u'price_unit': 25.0}], u'user_id': 1, u'amount_total': 10.0}

        def test_before_create_entity(cr, uid, model_name, entity=None, additional_values=None, **kwargs):
            if model_name == "sale.order":
                final_dict[0] = entity
            return entity

        self.edi_system.before_create_entity = test_before_create_entity
        so_model_id = self.registry('ir.model').search(cr, uid, [('model', '=', 'sale.order')])[0]
        pattern_id = self.pattern_model.create(cr, uid, {'name': 'Simple SO Pattern',
                                                         'model_id': so_model_id,
                                                         'json_pattern': JSON_PATTERN})

        self.assertIsNotNone(pattern_id)
        try:
            self.registry('edi_system').deserialize(cr, uid, pattern_id, XML_SO)
        except Exception, e:
            # Required field not found
            pass
        print final_dict[0]
        self.assertDictContainsSubset(correct_dict, final_dict[0])

    def test_serialize(self):
        cr, uid = self.cr, self.uid
        so_model_obj = self.registry('sale.order')
        order_lines = []
        val = {
            'name': 'test line',
            'product_uom_qty': 10,
            'product_id': 1,
            'product_uom': 1,
            'price_unit': 100,
        }
        val2 = {
            'name': 'test line 2',
            'product_uom_qty': 10,
            'product_id': 1,
            'product_uom': 1,
            'price_unit': 500,
        }
        order_lines.append((0, 0, val))
        order_lines.append((0, 0, val))
        order_lines.append((0, 0, val2))

        order_id = so_model_obj.create(cr, uid, {
            'name': 'test order',
            'user_id': uid,
            'partner_id': 1,
            'partner_invoice_id': 1,
            'partner_shipping_id': 1,
            'pricelist_id': 1,
            'order_line': order_lines,
            'order_policy': 'manual',
        })
        self.assertIsNotNone(order_id)

        so_model_id = self.registry('ir.model').search(cr, uid, [('model', '=', 'sale.order')])[0]
        pattern_id = self.pattern_model.create(cr, uid, {'name': 'Simple SO Pattern',
                                                         'model_id': so_model_id,
                                                         'json_pattern': JSON_PATTERN_SERIALIZE})

        # (self, cr, uid, pattern_id, document_id, partner_id=None, preview=None, log_cr=None, log_id=None, add_tag_if_empty=True, context=None):
        xml = self.registry('edi_system').serialize(cr, uid, pattern_id, order_id)
        print xml
        self.assertIsNotNone(xml)
openerp.domain_generator = function(instance) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.domain_generator.DomainGenerator = instance.web.form.AbstractField.extend({
        template: "DomainGenerator.widget",
        _in_drawer: true,
        init: function(parent, node) {
            this._super.apply(this, arguments);
            this.ParentViewManager = parent;
            this.node = _.clone(node);
        },
        start: function () {
            var self = this;
            this._super.apply(this, arguments);

            this.$el
                .on('keypress keydown keyup', function (e) { e.stopPropagation(); })
                .on('click', 'h4', function () {
                    self.$el.toggleClass('oe_opened');
                }).on('click', 'button.oe_add_condition', function () {
                    self.append_proposition();
                });

            $('button.oe_abstractformpopup-form-save, button.oe_form_button_save, button.oe_abstractformpopup-form-save-new')
                .focusin(function() {
                    self.commit_search();
                })
        },
        render_value: function() {
            var self = this;
            this._super.apply(this, arguments);
            this.record_id = this.view.datarecord.id;

            var mode = this.view.get("actual_mode");
            if ($.inArray(mode, ['edit', 'create']) != -1) {
                this.$el.find('button.oe_add_condition').show();
            } else {
                this.$el.find('button.oe_add_condition').hide();
            }

            if ($.inArray(this.field.type, ['char', 'text']) == -1){
                alert('You can only apply this widget on a char or a text field');
                this.$el.hide();
                return
            }

            //Check if you have the option "linked_field"
            //This option link the field char (how contains the domain) and a field many2many
            if (this.options.model) {
                this.field_model = this.options.model;
                new instance.web.Model('ir.model').call('search', [[['model', '=', this.field_model]]])
                    .done(function (data) {
                        if (data.length == 0){
                            alert('The model \'' + this.field_model + '\' doesn\'t exist in the database');
                            this.$el.hide();
                            return
                        }
                        new instance.web.Model('ir.model').call('read', [data, ['name']]).done(function(result) {
                            var model_name = result[0].name;
                            self.$el.find('h4.domain_generator_title').text('Search on ' + model_name);
                        });
                    });
            } else if (this.options.linked_field) {
                var field_name = this.options.linked_field;
                var field_linked = this.field_manager.fields[field_name];

                if (!field_linked || !field_linked.field) {
                    alert('This field ' + field_linked + ' not found in this view');
                    this.$el.hide();
                    return
                }

                var field_type = field_linked.field.type;
                if (field_type == 'selection') {
                    if (!field_linked.get('value')) {
                        alert('No value for the field selection');
                        this.$el.hide();
                        return
                    }

                    this.field_model = field_linked.get('value');
                    new instance.web.Model('ir.model').call('search', [[['model', '=', this.field_model]]])
                    .done(function (data) {
                        if (data.length == 0){
                            alert('The model \'' + this.field_model + '\' doesn\'t exist in the database');
                            this.$el.hide();
                            return
                        }
                    });

                } else if ($.inArray(field_type, ['many2many', 'one2many', 'many2one']) != -1) {
                    if (!field_linked.field.relation) {
                        alert('Model not found for the field \'' + field_linked + '\'');
                        this.$el.hide();
                        return
                    }
                    this.field_model = field_linked.field.relation;
                } else {
                    alert('This field ' + field_linked + ' is not a correct field');
                    this.$el.hide();
                    return
                }
            } else {
                alert('You have to add an option on this field. You can choice between:' +
                '\n- model : model_name' +
                '\n- linked_field : field_name');
                this.$el.hide();
                return
            }

            return $.when(
                this._super(),
                new instance.web.Model(this.field_model).call('fields_get', {
                        context: this.view.dataset.context
                    }).done(function(data) {
                        self.fields = {
                            id: { string: 'ID', type: 'id', searchable: true }
                        };
                        _.each(data, function(field_def, field_name) {
                            if (field_def.selectable !== false && field_name != 'id') {
                                self.fields[field_name] = field_def;
                            }
                        });
            })).done(function () {
                var current_domain = self.get('value');
                if (current_domain && current_domain != '[]') {
                    self.generate_proposition(current_domain, mode);
                }
            });
        },
        remove_all_proposition: function() {
            var children = this.getChildren();
            _.invoke(children, 'destroy');
        },
        generate_proposition: function(domain_str, mode) {
            var self = this;

            var rules = instance.web.pyeval.eval('domains', [domain_str]);
            console.log(rules);
            _.each(rules, function(rule){
                if(_.isString(rule)){
                    return;
                }

                if (rule.length != 3) {
                    console.log('Wrong rule');
                    return;
                }

                var regex = /^'(.*)'$/;

                var field_name = rule[0].replace(regex, "$1");
                var operator = rule[1].replace(regex, "$1");
                if (_.isString(rule[2])) {
                    var value = rule[2].replace(regex, "$1");
                } else {
                    var value = rule[2];
                }

                var field = self.fields[field_name];
                if (!field) {
                    console.log('Field not found');
                    return;
                }

                var field = self.fields[field_name];
                var field_type = field.type;

                var need_value = true;
                if (field_type != 'boolean' && !value) {
                    need_value = false;
                    if (operator == '=') {
                        operator = '∄';
                    } else {
                        operator = '∃';
                    }
                } else if(field_type == 'boolean') {
                    need_value = false;
                }


                if ($.inArray(mode, ['edit', 'create']) == -1) {
                    var Field = instance.web.search.custom_filters.get_object(field_type);
                    if(!Field) {
                        Field = instance.web.search.custom_filters.get_object("char");
                    }
                    this.value = new Field(self, field);
                    _.each(this.value.operators, function(operator_temp) {
                        if (operator_temp.value === operator) {
                            operator = operator_temp;
                        }
                    });

                    var label = self.get_label(field, operator, value);
                    self.$('ul').append('<li>' + label + '</li>');
                } else {
                    var node = new instance.web.search.ExtendedSearchProposition(self, self.fields);
                    node.appendTo(self.$('ul')).done(function () {
                        node.$(".searchview_extended_prop_field").val(field_name);
                        node.$(".searchview_extended_prop_field").trigger('change');

                        node.$(".searchview_extended_prop_op").val(operator);
                        node.$(".searchview_extended_prop_op").trigger('change');

                        if (need_value) {
                            node.$(".searchview_extended_prop_value").find('input').attr('value', value);
                        }
                    });
                }
            });


        },
        get_label: function (field, operator, value) {
            var format;
            switch (operator.value) {
                case '∃': case '∄': format = _t('%(field)s %(operator)s'); break;
                default: format = _t('%(field)s %(operator)s "%(value)s"'); break;
            }

            return _.str.sprintf(format, {
                field: field.string,
                // According to spec, HTMLOptionElement#label should return
                // HTMLOptionElement#text when not defined/empty, but it does
                // not in older Webkit (between Safari 5.1.5 and Chrome 17) and
                // Gecko (pre Firefox 7) browsers, so we need a manual fallback
                // for those
                operator: operator.label || operator.text || operator,
                value: value
            });
        },
        append_proposition: function () {
            var self = this;
            return (new instance.web.search.ExtendedSearchProposition(this, this.fields))
                .appendTo(this.$('ul')).done(function () {
                    self.$('button.oe_apply').prop('disabled', false);
                });
        },
        remove_proposition: function (prop) {
            // removing last proposition, disable apply button
            if (this.getChildren().length <= 1) {
                this.$('button.oe_apply').prop('disabled', true);
            }
            prop.destroy();
        },
        commit_search: function () {
            // Get domain sections from all propositions
            var children = this.getChildren();
            var propositions = _.invoke(children, 'get_proposition');
            var domain = _(propositions).pluck('value');
            // To avoid a infinite loop
            if (domain.length == 0) {return}
            for (var i = domain.length; --i;) {
                domain.unshift('|');
            }

            var active_id = this.record_id;
            var model = this.view.model;
            var field_name = this.name;
            var values = {};
            values[field_name] = domain;

            this.internal_set_value(domain);
        }
    });
    instance.web.form.widgets.add('domain_generator_widget', 'instance.domain_generator.DomainGenerator');
};
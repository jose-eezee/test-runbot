# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-It
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import re


def eval_domain_str(domain_str):
    full_domain = []

    if not domain_str:
        return []

    # We remove the first and the last character.
    # [['a', '=', 5]] => ['a', '=', 5]
    domain_str = domain_str[1:-1]
    # We cut the string to find all domain_operator and domain
    # A domain_operator can be '|' or '&'
    # A domain must is a string between []
    for domain_operator, domain in re.findall(r'\'([\||&])\'|(\[.*?\])', domain_str, re.M):
        if domain:
            domain_reg = re.match(r'\[(.*),\s?(.*),\s?(.*)\]', domain)
            if domain_reg:
                field_name = str(domain_reg.group(1)[1:-1])
                operator = str(domain_reg.group(2)[1:-1])
                value = str(domain_reg.group(3))
                if re.match(r'\d+', value):
                    value = int(value)
                elif re.match(r'\d+\.\d+', value):
                    value = float(value)
                elif re.match(r'^True|False$', value):
                    if value == 'True':
                        value = True
                    else:
                        value = False
                else:
                    value = str(value[1:-1])
                full_domain.append([field_name, operator, value])
        if domain_operator:
            full_domain.append(str(domain_operator))
    return full_domain


def merge_domains(domains_list=[]):
    result = []
    size = len(domains_list)

    if size == 0:
        raise Exception('You have to pass at least one domain')
    if size == 1:
        return domains_list[0]

    for index in range(0, size-1):
        result.append('&')

    for domain in domains_list:
        if not isinstance(domain, list):
            raise Exception('The argument %s is not a list' % domain)
        if not domain:
            result.pop(0)
            continue
        result += domain

    return result
# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-It
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Export Data to Winbooks',
    'version': '2.0',
    'category': 'Generic Modules/Tools',
    'description': """
        This module is to export data to a csf and act file
    """,
    'author': 'Eezee-It',
    "website": "http://www.eezee-it.com",
    'depends': ['account_detailed_invoice_taxes', 'document'],
    'data': [
            'security/ir.model.access.csv',
            'view/export_to_csv_view.xml',
            'view/account_tax.xml',
            'view/res_partner.xml'
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

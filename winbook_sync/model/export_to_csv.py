# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-It
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, fields, api, _

from operator import attrgetter

import csv
import base64
import cStringIO
import re
from unicodedata import normalize

DOCTYPE = {
    'out_invoice': '1',
    'out_refund': '1',
    'in_invoice': '2',
    'in_refund': '2',
}

FIELD_NAMES_CSF = [
    'number', 'type', 'name1', 'name2', 'civname1', 'civname2', 'adress1',
    'adress2', 'vatcat', 'country', 'vatnumber', 'paycode', 'telnumber',
    'faxnumber', 'bnkaccnt', 'zipcode', 'city', 'deflpost', 'lang',
    'category', 'central', 'vatcode', 'currency', 'lastremlev', 'lastremdat',
    'totdeb1', 'totcre1', 'totdebtmp1', 'totcretmp1', 'totdeb2', 'totcre2',
    'totdebtmp2', 'totcretmp2', 'islocked', 'memotype', 'isdoc', 'f28150',
    'wbmodified', ]

FIELD_NAMES_ACT = [
    'doctype', 'dbkcode', 'dbktype', 'docnumber', 'docorder',
    'opcode', 'accountgl', 'accountrp', 'bookyear', 'period',
    'date', 'datedoc', 'duedate', 'comment', 'commenttext',
    'amount', 'amounteur', 'vatbase', 'vatcode', 'curramount',
    'currcode', 'cureurbase', 'vattax', 'vatimput', 'currate',
    'remindlev', 'matchno', 'olddate', 'ismatched', 'islocked',
    'isimported', 'ispositive', 'istemp', 'memotype', 'isdoc',
    'docstatus', 'dicfrom', 'codakey', ]


class ExportConfig(models.Model):
    _name = "export.data.config"

    # Defaults
    def _get_start_date(self):
        last_export = self.search([], order='end_date DESC', limit=1)
        return last_export and last_export.end_date or fields.Datetime.now()

    # Fields
    name = fields.Char('Name', size=32, help="Folder name")
    create_date = fields.Datetime(
        'Creation Date', readonly=True, select=True, help="Date on creation.")
    export_date = fields.Datetime(
        'Last Export Date', readonly=True, default=_get_start_date)
    start_date = fields.Date(
        'Invoice start Date', required=True, default=_get_start_date)
    end_date = fields.Date(
        'Invoice end Date', required=True, default=fields.Datetime.now())

    def _get_act_dict(self, invoice):
        res = {}
        # By default make everything None and then fill only what we need
        for fn in FIELD_NAMES_ACT:
            res[fn] = None
        res['doctype'] = DOCTYPE.get(invoice.type)
        res['dbkcode'] = invoice.journal_id.code
        partner = invoice.partner_id.parent_id or invoice.partner_id
        res['accountrp'] = partner.winbooks_ref
        doc_period = invoice.period_id
        doc_fiscal_year = doc_period.fiscalyear_id
        periods = doc_fiscal_year.period_ids.filtered(
            lambda r: not r.special)
        periods = periods.sorted(key=attrgetter('date_start'))
        period_ids = [p.id for p in periods]
        res['period'] = '%02d' % (period_ids.index(doc_period.id)+1, )
        res['date'] = invoice.date_invoice
        res['datedoc'] = invoice.date_invoice
        res['duedate'] = invoice.date_due
        # DOCNUMBER
        # As DOCNUMBER max length is 8 char we take year and number
        # i.e. SAJ/2015/12334 -> 20151234
        number = invoice.number.split('/')
        res['docnumber'] = '%s%s' % (number[1], number[2])
        return res

    def _get_account_gl(self, record):
        """
        Get Winbooks code from current record, can be invoice or invoice.line
        :param record: recordset
        :return: char, char
        """
        account_mapping = self.env['account.mapping'].search([
            ('account_id', '=', record.account_id.id)])
        code = account_mapping and account_mapping.winbook_code or \
            record.account_id.code
        comment = normalize('NFKD', record.account_id.name).encode('ascii',
                                                                   'ignore')
        return code, comment

    def _get_amount_eur(self, record):
        """
        Positive amount = allocation of debit
        Negative amount = allocation of credit
        Total sum of amount should be 0 (debit = credit)
        :param record: recordset
        :return: float
        """
        if record._name == 'account.invoice':
            doc_type = record.type
            amount = record.amount_total
            factor = 1
        else:
            doc_type = record.invoice_id.type
            factor = -1
            if record._name == 'account.invoice.line':
                amount = record.price_subtotal
            else:
                amount = record.amount

        amount *= factor
        # Invert entry
        if doc_type in ['out_refund', 'in_invoice']:
            amount *= -1

        return amount

    def _get_vat_base(self, record):
            """
            Amount of the VAT base when encoding VAT or total turnover
            :param record: recordset
            :return: float
            """
            amount = None
            if record._name == 'account.invoice':
                amount = record.amount_untaxed
            elif record._name == 'account.invoice.tax':
                amount = record.base
            return amount

    # Methods
    @api.multi
    def export_data_scheduler(self):
        self.ensure_one()
        self.export_data_to_csf()
        self.export_data_to_act()
        return True

    @api.multi
    def export_data_to_csf(self):
        self.ensure_one()

        vals = {
            'name': 'CSF.txt',
            'type': 'csf',
            'start_date': self.start_date,
            'end_date': self.end_date,
            'state': 'success'
        }

        invoices = self.env['account.invoice'].search([
            ('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['paid', 'open'])
        ])

        if invoices:
            fd = cStringIO.StringIO()
            csv_writer = csv.DictWriter(fd, fieldnames=FIELD_NAMES_CSF,
                                        delimiter=',')
            tmpl_row = {}
            # By default make everything None and then fill only what we need
            for fn in FIELD_NAMES_CSF:
                tmpl_row[fn] = None

            partners = []
            for inv in invoices:
                p = inv.partner_id.parent_id or inv.partner_id
                if not p.winbooks_ref:
                    p.winbooks_ref = 'O%09d' % p.id
                partners.append(p)

            partners = list(set(partners))
            for partner in partners:
                row = tmpl_row.copy()
                row['number'] = partner.winbooks_ref
                row['type'] = partner.customer and '1' or '2'
                row['name1'] = normalize('NFKD',
                                         partner.name).encode('ascii',
                                                              'ignore') or None
                row['adress1'] = partner.street and \
                    normalize('NFKD',
                              partner.street).encode('ascii', 'ignore') or None
                row['vatcat'] = partner.vat and '1' or '3'
                if partner.country_id and partner.country_id.code:
                    row['country'] = partner.country_id.code
                if partner.vat:
                    vat = partner.vat
                    if re.match(r'\D{2}\d+', vat):
                        row['vatnumber'] = vat[2:]
                    else:
                        row['vatnumber'] = vat
                row['zipcode'] = partner.zip or None
                # city
                row['city'] = partner.city and \
                      normalize('NFKD',
                                partner.city).encode('ascii', 'ignore') or None

                # Writing the individual row to CSF file
                csv_writer.writerow(row.copy())
            del csv_writer
            results = base64.b64encode(fd.getvalue())
            self.env['ir.attachment'].create({
                'name': "CSF.txt",
                'datas': results,
                'datas_fname': "CSF.txt",
                'res_model': 'export.data.config',
                'res_id': self.id})
            
            fd.close()

        # writing to log file if no file entry found
        else:
            vals.update({'state': 'fail'})
        self.env['export.data.log'].create(vals)

    @api.multi
    def export_data_to_act(self):
        self.ensure_one()

        invoices = self.env['account.invoice'].search([
            ('date_invoice', '>=', self.start_date),
            ('date_invoice', '<=', self.end_date),
            ('state', 'in', ['paid', 'open'])])

        fd = cStringIO.StringIO()
        csv_writer = csv.DictWriter(fd, FIELD_NAMES_ACT)
        rows = []
        # Write invoice header
        for invoice in invoices:
            tmpl_row = self._get_act_dict(invoice)
            tmpl_row['amount'] = 0

            row = tmpl_row.copy()
            row['accountgl'], row['comment'] = self._get_account_gl(invoice)
            row['amounteur'] = self._get_amount_eur(invoice)
            row['vatbase'] = self._get_vat_base(invoice)
            rows.append(row.copy())

            # Write invoice lines
            for line in invoice.invoice_line:
                row = tmpl_row.copy()
                row['doctype'] = '3'
                row['accountgl'], row['comment'] = self._get_account_gl(line)
                row['amounteur'] = self._get_amount_eur(line)
                row['vatbase'] = self._get_vat_base(line)
                rows.append(row.copy())
            # Write invoice taxes
            for tax in invoice.tax_line:
                row = tmpl_row.copy()
                row['doctype'] = tax.amount == 0.0 and '4' or '3'
                row['accountgl'], row['comment'] = self._get_account_gl(tax)
                row['amounteur'] = self._get_amount_eur(tax)
                row['vatbase'] = self._get_vat_base(tax)
                row['vatcode'] = tax.account_tax_id.winbook_tax_code or None
                rows.append(row.copy())
        csv_writer.writerows(rows)
        results = base64.b64encode(fd.getvalue())
        self.env['ir.attachment'].create({
            'name': "ACT.txt",
            'datas': results,
            'datas_fname': "ACT.txt",
            'res_model': 'export.data.config',
            'res_id': self.id})
        fd.close()

        # writing to log file
        vals = {
            'name': 'ACT.txt',
            'type': 'act',
            'start_date': self.start_date,
            'end_date': self.end_date,
            'state': 'success'
        }
        if not rows:
            vals.update({'state': 'fail'})
        self.env['export.data.log'].create(vals)


class ExportLog(models.Model):
    _name = "export.data.log"

    name = fields.Char('Name')
    type = fields.Selection(selection=[('csf', _('Customers/Suppliers form')),
                                       ('act', _('Record files'))])
    start_date = fields.Datetime('Start date')
    end_date = fields.Datetime('End date')
    state = fields.Selection(oldname='success',
                             selection=[('success', _('Success')),
                                        ('fail', _('Failure'))])


class AccountMapping(models.Model):
    _name = "account.mapping"
    _rec_name = 'description'

    account_id = fields.Many2one('account.account', 'Account', required=True)
    description = fields.Char('Description', related='account_id.name')
    winbook_code = fields.Char('Winbooks code', size=128,
                               required=True)

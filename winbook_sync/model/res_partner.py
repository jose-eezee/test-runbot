# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-It
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.fields import Char
from openerp.models import Model


class ResPartner(Model):
    _inherit = 'res.partner'

    winbooks_ref = Char(oldname='accounting_ref', string='Winbooks ref')

    _sql_constraints = [
        ('winbooks_ref_unique', 'unique(winbooks_ref)',
         'Winbooks reference must be unique!'), ]

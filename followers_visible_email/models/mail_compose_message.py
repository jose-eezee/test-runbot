# -*- encoding: utf-8 -*-
##############################################################################
#
#   Copyright Eezee-It
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.models import Model
from openerp.fields import Boolean, Char
from openerp.api import one, depends


class MailComposeMessage(Model):
    _inherit = 'mail.compose.message'

    @one
    @depends('composition_mode')
    def _compute_lst_email_followers(self):
        context = self._context
        logged_user = self.env.user.partner_id
        values = []
        lst_emails = ''
        if self.composition_mode == 'comment':
            if any(val in context for val in ['active_model', 'default_model',
                                              'active_id', 'default_res_id']):
                active_model_obj = self.env[
                    context.get('active_model') or context.get(
                        'default_model')]
                active_model_id = active_model_obj.browse(
                    context.get('active_id') or context.get('default_res_id'))
                if active_model_id:
                    for rec in active_model_id:
                        emails = [val.email for val in rec.message_follower_ids
                                  if val.notify_email == 'always' and
                                  val.email and val.id != logged_user.id]
                        values.extend(emails)
            if values:
                lst_emails = lst_emails.replace(
                    '', '[%s] ' % ', '.join(values))
        self.lst_email_followers = lst_emails

    to_followers = Boolean(help='Option to or not include the followers of '
                           'document as the email recipients')
    lst_email_followers = Char(compute='_compute_lst_email_followers')

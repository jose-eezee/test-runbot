# -*- encoding: utf-8 -*-
##############################################################################
#
#   Copyright Eezee-It
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.models import Model
from openerp.api import v7
from openerp import SUPERUSER_ID


class MailMessage(Model):
    _inherit = 'mail.message'

    @v7
    def _notify(self, cr, uid, newid, context=None,
                force_send=False, user_signature=True):
        ''' Overrided method
                Customization of followers notification
            This action depends on 'to_followers' value defined in the
            mail.message.compose record.
            [False]:
                - Only take into account the recipients
                  define in the 'Recipients' fields.
            [True]:
                - Do nothing, take the default behaviour.
        '''
        context = dict(context or {})
        notification_obj = self.pool.get('mail.notification')
        msg_compose_obj = self.pool.get('mail.compose.message')
        partners_to_notify = set([])

        # mail.message id
        message_id = self.browse(cr, uid, newid, context)
        # get the email id just created
        msg_compose_id = msg_compose_obj.search(
            cr, uid, [('res_id', '=', message_id.res_id)], context=context)
        # all followers of the mail.message document have to be added as
        # partners and notified if a subtype is defined(otherwise: log message)
        if message_id.subtype_id and message_id.model and message_id.res_id:
            fol_obj = self.pool.get('mail.followers')
            # browse as SUPERUSER because rules could
            # restrict the search results
            fol_ids = fol_obj.search(cr, SUPERUSER_ID, [
                ('res_model', '=', message_id.model),
                ('res_id', '=', message_id.res_id)
            ], context=context)
            partners_to_notify |= set(
                fo.partner_id.id for fo in fol_obj.browse(
                    cr, SUPERUSER_ID, fol_ids, context=context)
                if message_id.subtype_id.id in [
                    st.id for st in fo.subtype_ids]
            )
            if msg_compose_id:
                # sorting the message
                msg_compose_id_sorted = sorted(msg_compose_id)
                # validate the newest message just created
                msg_compose_rec = msg_compose_obj.browse(
                    cr, uid, msg_compose_id_sorted[-1], context=context)
                if not msg_compose_rec.to_followers:
                    # reset the partner to notify
                    partners_to_notify &= set([])
                    # update it with a new value
                    partners_to_notify |= set(
                        partner.id for partner in msg_compose_rec.partner_ids)
        # remove me from notified partners, unless the message is written on my
        # own wall
        if message_id.subtype_id and message_id.author_id and \
                message_id.model == "res.partner" and \
                message_id.res_id == message_id.author_id.id:
            partners_to_notify |= set([message_id.author_id.id])
        elif message_id.author_id:
            partners_to_notify -= set([message_id.author_id.id])

        # all partner_ids of the mail.message have to be notified regardless of
        # the above (even the author if explicitly added!)
        if message_id.partner_ids:
            partners_to_notify |= set([p.id for p in message_id.partner_ids])

        # notify
        notification_obj._notify(
            cr, uid, newid,
            partners_to_notify=list(partners_to_notify), context=context,
            force_send=force_send, user_signature=user_signature
        )
        message_id.refresh()

        # An error appear when a user receive a notification without notifying
        # the parent message -> add a read notification for the parent
        if message_id.parent_id:
            # all notified_partner_ids of the mail.message have to be notified
            # for the parented messages
            partners_to_parent_notify = set(message_id.notified_partner_ids)\
                .difference(message_id.parent_id.notified_partner_ids)
            for partner in partners_to_parent_notify:
                notification_obj.create(cr, uid, {
                    'message_id': message_id.parent_id.id,
                    'partner_id': partner.id,
                    'is_read': True,
                }, context=context)

# -*- encoding: utf-8 -*-
##############################################################################
#
#   Copyright Eezee-It
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Followers visible e-mail',
    'version': '0.1',
    'author': 'Eezee-It',
    'website': 'http://www.eezee-it.com',
    'category': 'mail',
    'description': '''
Followers visible e-mail
========================

This module will modify the standard messaging behaviour as follows:
By default, followers will not get a notification when sending a message
for the current object.
Only the partners (e-mail addresses) displayed in the recipients field
will receive it.

A checkbox allows to switch to standard behaviour
for every message being composed (i.e. notify followers and recipients).
We display the e-mail addresses of the followers (partners)
with "Receive Inbox Notifications by Email" set to "All Messages"
in the message composition screen.
This allows to see who will get notified (and e-mailed)
when the checkbox is marked.
''',
    'depends': ['mail'],
    'data': [
        'views/mail_compose_message_view.xml',
        'views/mail_thread.xml'
    ],
    'installable': True
}

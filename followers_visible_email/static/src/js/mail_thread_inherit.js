
function openerp_follower_visible_email(instance, module){

	module.ThreadComposeMessage = module.ThreadComposeMessage.extend({
		bind_events: function() {
			this._super();
			this.$('.oe_compose_post').on('click', _.bind(this.on_compose_fullmail, this, this.id ? 'reply' : 'comment'));
		},
	});

}

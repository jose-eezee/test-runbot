from openerp import models, api, _
from openerp.exceptions import except_orm
from openerp.tools import float_compare

import logging

logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def check_tax_lines(self, compute_taxes):
        logging.debug('Checking tax lines with detailed method')
        account_invoice_tax = self.env['account.invoice.tax']
        company_currency = self.company_id.currency_id
        if not self.tax_line:
            for tax in compute_taxes.values():
                account_invoice_tax.create(tax)
        else:
            tax_key = []
            precision = self.env['decimal.precision'].precision_get('Account')
            for tax in self.tax_line:
                if tax.manual:
                    continue
                key = (tax.tax_code_id.id, tax.base_code_id.id,
                       tax.account_id.id, tax.account_tax_id.id)
                tax_key.append(key)
                if key not in compute_taxes:
                    # If key is not in taxes it might be possible that tax
                    # lines were computed using default grouped method, in that
                    # case we need to call super and invoke grouped method
                    super(AccountInvoice, self).check_tax_lines(compute_taxes)
                    return
                base = compute_taxes[key]['base']
                if float_compare(abs(base - tax.base),
                                 company_currency.rounding,
                                 precision_digits=precision) == 1:
                    raise except_orm(_('Warning!'),
                                     _('Tax base different!\nClick on compute '
                                       'to update the tax base.'))
            for key in compute_taxes:
                if key not in tax_key:
                    raise except_orm(_('Warning!'),
                                     _('Taxes are missing!\nClick on compute '
                                       'button.'))

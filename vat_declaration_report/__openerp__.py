# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-It
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Report VAT Declaration',
    'version': '1.0',
    'author': 'Eezee-It',
    'category': 'Localization/Account Charts',
    'website': 'http://www.eezee-it.com',
    'description': """
PDF file of periodical VAT declaration :
========================================
This module adds a PDF file report for the VAT Declaration.
It depends on l10n_be module and adds a button to print the report in the existing wizard 'Periodical VAT Declaration'

**Path to access :** Invoicing/Reporting/Legal Reports/Belgium Statements/Periodical VAT Declaration""",
    'depends': ['base', 'l10n_be'],
    'init': [],
    'demo': [],
    'data': [
        "wizard/l10n_be_account_vat_declaration.xml",
        "report/template_vat_declaration.xml",
    ],
    'active': False,
    'installable': True,
    'images': ['images/menu.PNG', 'images/wizard.PNG'],
}

# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright Eezee-It
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp import models, api
from openerp.osv import osv
from openerp.tools.translate import _


class VatDeclaration(models.TransientModel):
    _inherit = 'l1on_be.vat.declaration'

    @api.multi
    def create_pdf(self):
        data = self.vat_declaration()[0]
        return self.env['report'].get_action(self, 'vat_declaration_report.report_periodical_vat_declaration',
                                             data=data)

    @api.one
    def vat_declaration(self):
        list_of_tags = ['00', '01', '02', '03', '44', '45', '46', '47', '48', '49', '81', '82', '83', '84', '85', '86',
                        '87', '88', '54', '55', '56', '57', '61', '63', '65', 'XX', '59', '62', '64', '66', 'YY', '71',
                        '72', '91', 'VI']

        if self.tax_code_id:
            company = self.tax_code_id.company_id
        else:
            company = self.env.user.company_id
        vat_no = company.partner_id.vat
        if not vat_no:
            raise osv.except_osv(_('Insufficient Data!'), _('No VAT number associated with your company.'))
        vat_no = vat_no.replace(' ', '').upper()

        data = self.read()[0]
        new_tax_code_ids = self.env['account.tax.code'].\
            with_context(period_id=self.period_id.id).search([('code', 'in', list_of_tags)])
        tax_infos = new_tax_code_ids.with_context(period_id=self.period_id.id).read(['code', 'sum_period', 'name'])

        default_address = company.partner_id.address_get()
        address_id = self.env['res.partner'].search([('id', '=', default_address.get('default'))]) or \
                     company.partner_id
        issued_by = vat_no[:2]
        comments = data['comments'] or ''

        send_ref = str(company.partner_id.id) + str(self.period_id.date_start[5:7]) + \
                   str(self.period_id.date_stop[:4])

        starting_month = self.period_id.date_start[5:7]
        ending_month = self.period_id.date_stop[5:7]
        quarter = str(((int(starting_month) - 1) / 3) + 1)

        cases_list = []
        for item in tax_infos:
            if item['code'] == '91' and ending_month != 12:
                #the tax code 91 can only be send for the declaration of December
                continue
            if item['code'] == 'VI':
                if item['sum_period'] >= 0:
                    item['code'] = '71'
                else:
                    item['code'] = '72'
            if item['code'] in list_of_tags:
                cases_list.append(item)
        cases_list.sort()

        return {
            'issued_by': issued_by,
            'vat_no': vat_no,
            'only_vat': vat_no[2:],
            'cmpny_name': company.name,
            'address': '%s %s'%(address_id.street or '',address_id.street2 or ''),
            'post_code': address_id.zip or '',
            'city': address_id.city or '',
            'country': address_id.country_id.name or '',
            'country_code': address_id.country_id and address_id.country_id.code or '',
            'email': address_id.email or '',
            'phone': address_id.phone or '',
            'send_ref': send_ref,
            'quarter': quarter,
            'month': starting_month,
            'year': str(self.period_id.date_stop[:4]),
            'client_nihil': (data['client_nihil'] and 'YES' or 'NO'),
            'ask_restitution': (data['ask_restitution'] and 'YES' or 'NO'),
            'ask_payment': (data['ask_payment'] and 'YES' or 'NO'),
            'comments': comments,
            'period_id': self.period_id.name,
            'tax_infos': cases_list,
        }
# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2013 Eezee-it nv/sa (www.eezee-it.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.models import Model, api, _
from openerp.exceptions import Warning

class IrCron(Model):
    _inherit = "ir.cron"

    @api.multi
    def name_get(self):
        vals= []
        for cron in self:
            name = "%d. %s - %s.%s%s" % (cron.id, cron.name, cron.model, cron.function, cron.args)
            vals.append((cron.id, name))
        return vals

    @api.multi
    def execute_scheduler(self):
        try:
            print "Start %s" % self[0].name_get()
            self._callback(self[0].model, self[0].function, self[0].args, self[0].id)
        except:
            raise Warning("Error during the execution of the scheduler")

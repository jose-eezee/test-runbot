# -*- encoding: utf-8 -*-
##############################################################################
#
#    Author: Samuel Lefever
#    Copyright Eezee-it 2013
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from grammar import grammar
import re
from openpyxl import Workbook
from openpyxl import load_workbook
from copy import deepcopy
from openpyxl.styles import Style
from safe_eval import safe_eval as eval

class template_word(object):
    endloop = re.compile('^[\s]*endloop[(](.+)[)][\s]*$')
    string = re.compile('^[\s]*[\'](.*)[\'][\s]*$')

    def __init__(self, word):
        """
        Contructor of template_word

        Create a new xlsx method object

        :param word: the xlsx method
        :returns: a new template_word
        """
        self.word = word
        self.type = ''
        try:
            result = grammar.syntax.parseString(word)
            if result.type in ('hloop','vloop'):
                self.type = result.type
                self.key = result.key
                self.value_to_loop = result.value_to_loop
                self.content = result.content
            elif result.type == 'endloop':
                self.type = result.type
                self.content = result.content
                self.hide = result.hide
        except Exception as e:
            pass
        if self.type in ('hloop', 'vloop', 'endloop'):
            # print 'loop'
            pass
        elif self.string.match(word):
            matched = self.string.match(word)
            self.type = 'string'
            self.content = matched.group(1)

        else:
            self.type = 'other'


class loop(object):
    def __init__(self, sheet, word, x, y, parser):
        """
        Constructor of loop

        :param sheet: the worksheet
        :param word: the xlsx method object
        :param x: start x coordinate
        :param y: start y coordinate
        :param parser: the parser
        :returns:
        """
        self.x = x
        self.y = y
        self.word = word
        self.sheet = sheet
        self.parser = parser
        self.last_inside_loop = False
        self.last_coord = False
        self.stack = 0
        self.hide = False
        self.size = 0

    def count_cell(self, h, v, value=False):
        """
        count_cell method - count number of cell for a generic loop
        Until the current cell isn't the endloop method, the method return False

        :param h: start x coordinate
        :param v: start y coordinate
        :param value: the xlsx method (default = False)
        :returns: return False if the loop isn't finish
        """
        if not value:
            value = self.sheet.cell(column=h, row=v).value
            if isinstance(value, basestring):
                matched = re.match('<%(.*)%>', value)
                if matched:
                    value = matched.group(1)
        if value:
            word = template_word(value)
            if word.type == self.word.type:
                self.stack = self.stack + 1
            elif word.type == 'endloop':
                return self.end_loop(word)
        self.size = self.size + 1
        return False

    def end_loop(self, word):
        """
        end_loop method -

        :param word: the xlsx method object
        :returns: return False if the word isn't a endloop method, True if the word is a endloop method and the xlsx method object
        """
        if self.stack:
            self.stack = self.stack - 1
            content = template_word(word.content)
            if content.type == 'endloop':
                return self.end_loop(content)
            else:
                self.size = self.size + 1
                return False
        else:
            self.word.hide = word.hide
            return True

    def treat_loop_cells(self, dest_x, dest_y, z):
        raise NotImplementedError

    def manage_empty_loop(self, dest_x, dest_y):
        raise NotImplementedError

    def do_loop(self, dest_x, dest_y, dict):
        """
        do_loop method -


        :param dest_x:
        :param dest_y:
        :param dict:
        :returns:
        """
        value_to_loop = eval(self.word.value_to_loop, dict)
        if not len(value_to_loop):
            self.manage_empty_loop(dest_x, dest_y)
        else:
            for index in range(0, len(value_to_loop)):
                object = value_to_loop[index]
                dict[self.word.key] = object
                self.treat_loop_cells(dest_x, dest_y, index, dict)


class horizontal_loop(loop):

    def __init__(self, sheet, word, x, y, parser):
        """
        Constructor of horizontal_loop



        :param sheet:
        :param word:
        :param x:
        :param y:
        :param parser:
        :returns:
        """
        super(horizontal_loop, self).__init__(sheet, word, x, y, parser)
        self.count_cell(x, y, word.content)

    def manage_empty_loop(self, dest_x, dest_y):
        """
        manage_empty_loop method -

        :param dest_x:
        :param dest_y:
        :returns:
        """
        self.last_coord = dest_x - 1, dest_y
        self.hide = self.word.hide.strip() == 'True'
        for j in range(1, self.size):
            self.parser.set_treated_cells(self.x + j, self.y, self.x + j, self.y, 0)

    def treat_loop_cells(self, dest_x, dest_y, index, dict):
        """
        treat_loop_cells method -



        :param dest_x:
        :param dest_y:
        :param index:
        :param dict:
        :returns:
        """
        z = 0
        while(z < self.size):
            if self.last_inside_loop and self.last_inside_loop.word.type == self.word.type:
                z = z + self.last_inside_loop.size - 1
                dest_x = self.last_inside_loop.last_coord[0] + 1
                if self.last_inside_loop.hide:
                    self.parser.set_treated_cells(self.x + z, self.y, dest_x, dest_y)
                    z = z + 1
                    self.last_coord = self.last_coord[0] - 1, self.last_coord[1]
                self.last_inside_loop = False
                if z == self.size:
                    return
            else:
                dest_x = self.last_coord and self.last_coord[0] + 1 or dest_x
            if z == 0:
                # if dest_y != self.y and index == 0:
                #     dest_x = self.x
                self.parser.process_word(dest_x, dest_y, self.word.content, dict, self.x + z, self.y, self)
            else:
                value = self.sheet.cell(column=self.x + z, row=self.y).value
                self.parser.treat_value(value, dict, dest_x, dest_y, self.x + z, self.y, self)
            self.last_coord = dest_x, dest_y
            z = z + 1

    def count_cell(self, h, v, value = False):
        """
        count_cell -


        :param h:
        :param v:
        :param value:
        :returns:
        """
        stop = super(horizontal_loop, self).count_cell(h, v, value)
        if not stop:
            self.count_cell(h+1, v)

class vertical_loop(loop):

    def __init__(self, sheet, word, x, y, parser):
        """
        Constructor of vertical_loop. Create a vertical loop

        Call the super constructor and count number of cell

        :param sheet: the sheet proceeded
        :param word: the xlsx method object
        :param x: start x coordinate
        :param y: start y coordinate
        :param parser: the parser
        :returns:
        """
        super(vertical_loop, self).__init__(sheet, word, x, y, parser)
        self.count_cell(x, y, word.content)

    def manage_empty_loop(self, dest_x, dest_y):
        """
        manage_empty_loop method -

        :param dest_x:
        :param dest_y:
        :returns:
        """
        self.last_coord = dest_x, dest_y - 1
        self.hide = self.word.hide.strip() == 'True'
        for j in range(1, self.size):
            self.parser.set_treated_cells(self.x, self.y + j, self.x, self.y + j)

    def treat_loop_cells(self, dest_x, dest_y, index, dict):
        """
        treat_loop_cells method -

        :param dest_x:
        :param dest_y:
        :param index:
        :param dict:
        :returns:
        """
        z = 0
        while(z < self.size):
            if self.last_inside_loop and self.last_inside_loop.word.type == self.word.type:
                z = z + self.last_inside_loop.size - 1
                dest_y = self.last_inside_loop.last_coord[1] + 1
                if self.last_inside_loop.hide:
                    self.parser.set_treated_cells(self.x, self.y + z, dest_x, dest_y)
                    z = z + 1
                    self.last_coord = self.last_coord[0], self.last_coord[1] - 1
                self.last_inside_loop = False
                if z == self.size:
                    return
            else:
                dest_y = self.last_coord and self.last_coord[1] + 1 or dest_y
            if z == 0:
                # if dest_x != self.x and index == 0:
                #     dest_y = self.y
                self.parser.process_word(dest_x, dest_y, self.word.content, dict, self.x, self.y + z, self)
            else:
                value = self.sheet.cell(column=self.x, row=self.y + z).value
                self.parser.treat_value(value, dict, dest_x, dest_y, self.x, self.y + z, self)
            self.last_coord = dest_x, dest_y
            z = z + 1

    def count_cell(self, h, v, value=False):
        """
        count_cell method - count number of cells between the start y to the endloop method
        For each cell, the method call the generic method count_cell

        :param h: start y coordinate
        :param v: start x coordinate
        :param value: the xlsx method
        :returns:
        """
        stop = super(vertical_loop, self).count_cell(h, v, value)
        if not stop:
            self.count_cell(h, v+1)

class xlsx_sheet_parser(object):

    def __init__(self, sheet, result_sheet):
        """
        Constructor of xlsx_sheet_parser.

        :param sheet:
        :param result_sheet:
        :returns:
        """
        self.sheet = sheet
        self.result_sheet = result_sheet
        self.max_x = sheet.get_highest_column()
        self.max_y = sheet.get_highest_row()
        self.treated_cells = {y : {} for y in range(1, self.max_y + 2)}

    def retrieve_destination_coord(self, x, y):
        """
        retrieve_destination_coord method -

        :param x:
        :param y:
        :returns:
        """
        # dest_y = self.treated_cells.get(y, {}).get('dest_y', y)
        # dest_x = self.treated_cells.get(y, {}).get('dest_x', x)
        dest_y = self.treated_cells.get(y - 1, {}).get(x, (x,y-1))[1] + 1
        dest_x = self.treated_cells.get(y, {}).get(x - 1, (x-1,y))[0] + 1
        return dest_x, dest_y

    def process_word(self, dest_x, dest_y, temp_word, dict, start_x, start_y, loop = False):
        """
        process_word method - process a method according to the temp_word
        The temp_word will be split with the method template_word to find loop and methods

        :param dest_x: destination x coordinate
        :param dest_y: destination y coordinate
        :param temp_word: xlsx method to process
        :param dict: the localcontext
        :param start_x: start x coordinate
        :param start_y: start y coordinate
        :param loop: loop (default = False)
        :returns:
        """
        word = template_word(temp_word)
        if word.type == 'hloop':
            hloop = horizontal_loop(self.sheet, word, start_x, start_y, self)
            hloop.do_loop(dest_x, dest_y, dict)
            if loop:
                loop.last_inside_loop = hloop
        elif word.type == 'vloop':
            vloop = vertical_loop(self.sheet, word, start_x, start_y, self)
            vloop.do_loop(dest_x, dest_y, dict)
            if loop:
                loop.last_inside_loop = vloop
        elif word.type == 'endloop':
            self.process_word(dest_x, dest_y, word.content, dict, start_x, start_y, loop)
        elif word.type == 'string':
            self.set_cell_value(dest_x, dest_y, word.content, start_x, start_y)
        else:
            try:
                value = eval(word.word, dict)
            except Exception, e:
                print e.message
                print 'destination x,y : %d,%d' % (dest_x, dest_y)
                print 'from x,y : %d,%d' % (start_x, start_y)
                raise e
            self.treat_value(value, dict, dest_x, dest_y, start_x, start_y, loop)

    def set_treated_cells(self, start_x, start_y, dest_x, dest_y):
        """
        set_treated_cells method -

        :param start_x:
        :param start_y:
        :param dest_x:
        :param dest_y:
        :returns:
        """
        treated_line = self.treated_cells[start_y]
        treated_line[start_x] = dest_x, dest_y
        treated_line['dest_x'] = treated_line.get('dest_x',start_x + 1) < dest_x + 1 and dest_x + 1 or treated_line.get('dest_x',start_x + 1)
        next_y = self.treated_cells[start_y + 1].get('dest_y', False)
        if next_y <= dest_y:
            self.treated_cells[start_y + 1]['dest_y'] = dest_y + 1

    def set_cell_value(self, dest_x, dest_y, value, start_x, start_y ):
        """
        set_cell_value method -

        :param dest_x:
        :param dest_y:
        :param value:
        :param start_x:
        :param start_y:
        :returns:
        """

        self.result_sheet.cell(column=dest_x, row=dest_y).value = value
        self.set_treated_cells(start_x, start_y, dest_x, dest_y)
        # self.copy_style(self.result_workbook.cell(column=dest_x, row=dest_y).style, temp_style)

    def treat_value(self, value, dict, dest_x, dest_y, start_x, start_y, loop=False):
        """
        treat_value method -


        :param value: the xlsx method
        :param dict: the localcontext
        :param dest_x: destination x coordinate
        :param dest_y: destination y coordinate
        :param start_x: start x coordinate
        :param start_y: start y coordinate
        :param loop: loop (default = False)
        :returns:
        """
        if value or value != None and not isinstance(value, bool):
            if isinstance(value, basestring):
                template_word = re.match('<%(.*)%>', value)
                if template_word:
                    self.process_word(dest_x, dest_y, template_word.group(1), dict, start_x, start_y, loop)
                else:
                    self.set_cell_value(dest_x, dest_y, value, start_x, start_y)

            elif isinstance(value, int) or isinstance(value, long):
                self.set_cell_value(dest_x, dest_y, value, start_x, start_y)

            elif isinstance(value, float):
                self.set_cell_value(dest_x, dest_y, value, start_x, start_y)

            else:
                raise TypeError

    def parse(self, dict):
        """
        parse method - compute the destination coordinate according to the start coordinate

        :param dict: the localcontext
        :returns:
        """
        for y in range(1, (self.max_y + 1)):
            for x in range(1, (self.max_x + 1)):
                if self.treated_cells.get(y).get(x, -1) != -1:
                    continue

                value = self.sheet.cell(column=x, row=y).value

                dest_x, dest_y = self.retrieve_destination_coord(x, y)
                self.treat_value(value, dict, dest_x, dest_y, x, y)

                copy_style = self.sheet.cell(column=x, row=y).style.copy()
                self.result_sheet.cell(column=dest_x, row=dest_y).style = copy_style

class xlsx_parser(object):

    def __init__(self, filename, destination_filename):
        """
        Constructor of xlsx_parser

        :param filename:
        :param destination_filename:
        :returns:
        """
        self.filename = filename
        self.destination_filename = destination_filename
        self.workbook = load_workbook(filename = self.filename)
        self.result_workbook = Workbook()

    def parse(self, dict):
        """
        parse method -

        :param dict:
        :returns:
        """
        for sheet in self.workbook.worksheets:
            result_sheet = self.create_sheet(sheet)
            sheet_parser = xlsx_sheet_parser(sheet, result_sheet)
            sheet_parser.parse(dict)
        self.result_workbook.save(filename = self.destination_filename)

    def create_sheet(self, sheet):
        """
        create_sheet method - Taking the sheet already existing in all new excel file

        :param sheet:
        :returns:
        """
        if self.workbook.get_index(sheet) == 0:
            result_sheet = self.result_workbook.worksheets[0]
            result_sheet.title = sheet.title
        # Creating a new sheet in the new excel file
        else:
            sheet_copy = self.result_workbook.create_sheet(self.workbook.get_index(sheet), sheet.title)
        return result_sheet


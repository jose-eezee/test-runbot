# -*- encoding: utf-8 -*-
##############################################################################
#
#    Author: Samuel Lefever
#    Copyright Eezee-it 2013
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

"""
xlsx parser - this parser allow to generate a xlsx report from a xlsx template

There is three main types of method in a xlsx template:
* <% method_name(params) %>
* <% value_return vloop(object_iterate, value_return.value_display) %>
* <% value_return hloop(object_iterate, value_return.value_display) %>

The method vloop and hloop must have a <% endloop('what_display') %>
The value_return.value_display can be another method
"""

import cStringIO
import os
from openerp import report
from openerp.report.report_sxw import *
from openerp import pooler
from openerp.tools.translate import _
from openerp.modules import module
from openerp.osv.osv import except_osv
from openerp.http import request
import logging
from xlsx_parser import xlsx_sheet_parser
from openpyxl import Workbook
from openpyxl import load_workbook
_logger = logging.getLogger(__name__)

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

class report_xlsx(report_sxw):

    def __init__(self, name, table, rml=False, parser=False,
        header=True, store=False):
        """
        Constructor of report_xlsx

        @param name: the object name
        @param table: the model name
        @param rml: the path to the template (default = False)
        @param parser: the parser object (default = False)
        @param header: the header of the report (default = True)
        @param store: store (default = False)
        @return:
        """
        self.parser_instance = False
        self.localcontext = {}
        self.rml_template = rml
        report_sxw.__init__(self, name, table, rml, parser,
            header, store)

    def create(self, cr, uid, ids, data, context=None):
        """
        create method - Create a new parser according to the type of report (xlsx or not).
        The name of report is defined in the field self.name

        @param cr: cursor in db
        @param uid: id of current user
        @param ids: list of ids
        @param data: model name and xml_data
        @param context: context
        @return: a new report according to the type of report
        """
        pool = pooler.get_pool(cr.dbname)   
        report_obj = pool.get('ir.actions.report.xml')
        report_ids = report_obj.search(cr, uid,
                [('report_name', '=', self.name[7:])], context=context)
        if report_ids:
            report_xml = report_obj.browse(cr, uid, report_ids[0], context=context)
            self.title = report_xml.name
            if report_xml.report_type == 'controller':
                return self.create_source_xlsx(cr, uid, ids, data, report_xml, context)
        return super(report_xlsx, self).create(cr, uid, ids, data, context)


    def create_source_xlsx(self, cr, uid, ids, data, report_xml, context=None):
        """
        create_source_xlsx method - create a xlsx report according to a report_xml

        @param cr: cursor in db
        @param uid: id of current user
        @param ids: list of ids
        @param data: model name and xml_data
        @param report_xml: browse record of report xml
        @param context: context
        @return: return the xlsx file
        """
        if not context: context = {}
        context['active_ids'] = ids
        parser_instance = self.parser(cr, uid, self.name2, context)
        objs = self.getObjects(cr, uid, ids, context)
        parser_instance.set_context(objs, data, ids, 'xlsx')
        workbook_template = False

        if self.rml_template :
            path = module.get_module_resource(*self.rml_template.split(os.path.sep))
            if path and os.path.exists(path) :
                workbook_template = load_workbook(path)
        if not workbook_template :
            raise except_osv(_('Error!'), _('xlsx report template not found!'))

        result_workbook = Workbook()
        self.parse(parser_instance.localcontext, workbook_template, result_workbook)

        n = cStringIO.StringIO()
        result_workbook.save(n)
        n.seek(0)
        return (n.read(), 'xlsx')

    def parse(self, dict, workbook_template, result_workbook):
        """
        parse method - for each sheet in the workbook_template, the method call the method create_sheet and parse this new sheet

        @param dict: the localcontext
        @param workbook_template: xlsx template file
        @param result_workbook: a new instance of workbook that will contain the results
        @return:
        """
        for sheet in workbook_template.worksheets:
            result_sheet = self.create_sheet(sheet, workbook_template, result_workbook)
            sheet_parser = xlsx_sheet_parser(sheet, result_sheet)
            sheet_parser.parse(dict)

    def create_sheet(self, sheet, workbook_template, result_workbook):
        """
        create_sheet method - create a new sheet

        @param sheet: the sheet we want to parse
        @param workbook_template: xlsx template file
        @param result_workbook: a instance of workbook that will contain the results
        @return: return the result_sheet
        """
        # Taking the sheet already existing in all new excel file
        if workbook_template.get_index(sheet) == 0:
            result_sheet = result_workbook.worksheets[0]
            result_sheet.title = sheet.title
        # Creating a new sheet in the new excel file
        else:
            result_sheet = result_workbook.create_sheet(workbook_template.get_index(sheet), sheet.title)
        return result_sheet

    def generate_xlsx_report(self, cr, uid, ids, context=None):
        """
        Generate a new report and return this report with a HTTP response

        :param cr: cursor in db
        :param uid: id of current user
        :param ids: list of ids
        :param context: context
        :return: a http response with the report according to the type of report
        """

        report = self.create(cr, uid, ids, data={'report_type':'xlsx'}, context=context)

        file = report[0]
        file_type = report[1]

        file_header = [('Content-Type', 'application/%s' % file_type),
                          ('Content-Length', len(file)),
                          ('Content-Disposition', 'attachment; filename=%s.%s' % (self.title, file_type))]

        return request.make_response(file, headers=file_header)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

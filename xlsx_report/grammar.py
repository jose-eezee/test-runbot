__author__ = 'samuellefever'

from pyparsing import Literal,Word,ZeroOrMore,Forward,nums,oneOf,Group, CharsNotIn, alphas, Combine, OneOrMore, Optional, srange

"""

"""


class grammar(object):
    """
    Grammar object
    """

    def __init__(self):
        coma = Literal( ',' )
        lpar  = Literal( '(' )
        rpar  = Literal( ')' )

        param = CharsNotIn('(),')

        expr = Forward()
        elem = OneOrMore(Group( param + lpar + Optional(expr) + rpar ).leaveWhitespace() | param)
        expr << elem + ZeroOrMore( coma + expr ).leaveWhitespace()

        parameter = Combine(ZeroOrMore(param + ZeroOrMore(Group(lpar + Optional(expr) + rpar))))

        loop = Word(srange("[a-zA-Z0-9_]")).setResultsName('key') + oneOf('hloop vloop').setResultsName('type') + lpar.suppress() + parameter.setResultsName('value_to_loop') + coma.suppress() + parameter.setResultsName('content') + rpar.suppress()
        endloop = Literal('endloop').setResultsName('type') + lpar.suppress() + parameter.setResultsName('content') + Optional(Combine(coma.suppress() + parameter.setResultsName('hide'))) + rpar.suppress()
        self.syntax = loop | endloop

grammar = grammar()
# -*- coding: utf-8 -*-
##############################################################################
#
#    Eezee-It
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name" : "Manage Users",
	"version" : "0.1",
	"author" : "Eezee-It",
	"category" : "Generic Modules/Others",
	"website": "http://www.eezee-it.com",
	"description": "Allow to manage users for specifics users",
	"depends" : ["base"],
	"init_xml" : [],
	"demo_xml" : [],
	"data" : [
        "view/base_security.xml",
        "security/ir.model.access.csv",
        "security/ir_rule.xml",
    ],
	"active": False,
	"installable": True
}
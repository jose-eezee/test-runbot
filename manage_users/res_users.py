# -*- coding: utf-8 -*-
##############################################################################
#
#    Eezee-It
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.models import Model, api, _
from openerp import SUPERUSER_ID
from openerp.exceptions import Warning

class ResUsers(Model):
    _inherit = "res.users"

    @api.one
    @api.constrains('groups_id')
    def _check_groups(self):
        settings_groups_rec = self.env.ref('base.group_no_one')

        if self.env.uid == SUPERUSER_ID:
            return True

        if settings_groups_rec in self.groups_id:
            raise Warning('Error!\nYou cannot set the Settings group to a user')

    @api.model
    def fields_get(self, allfields=None, write_access=True, attributes=None):
        """
        In v8, the method field_get is overwrite for the model res_users.
        The method check if the user is Admin or has the group "Access Rights".
        Otherwise, the user has no access to fields with groups.
        """
        # We cannot use the object self.env.user because any method is call with sudo()
        if self.browse(self.env.uid).has_group('manage_users.group_manage_users'):
            return super(ResUsers, self.sudo()).fields_get(allfields=allfields, write_access=write_access, attributes=attributes)
        return super(ResUsers, self).fields_get(allfields=allfields, write_access=write_access, attributes=attributes)
openerp.web_rights_in_context = function (instance) {
    instance.web.View.include({
        is_action_enabled: function(action) {
            if(this.ViewManager.action){
                context = this.ViewManager.action.context;
                if(context[action] !== undefined) {
                    return context[action] == 1;
                }
            }
            return this._super(action);
        }
    });
}